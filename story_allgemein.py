# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# In der nachfolgenden Fallstudie werden wir mehrere Datensätze mit Tweets untersuchen. Dabei vergleichen wir Tweets von "normalen" Twitter-Nutzern mit Tweets von Donald Trum und Tweets von russischen Trollen.
#
# Als Datenquellen verwenden wir öffentlich zugängliche Datensätze von Tweets:
# * ["normale" Twitter-User](http://help.sentiment140.com/for-students/)
# * [Donald Trump](https://github.com/mkearney/trumptweets)
# * [russische Trolle](https://github.com/fivethirtyeight/russian-troll-tweets/)
#
# Der Inhalt der folgenden Tutorien gliedert sich in etwa wie folgt:
# * **Übung 05:** die Datensätze einlesen, aufräumen und grundlegende Eigenschaften der enthaltenen Informationen erkunden
# * **Übung 06:** Tweet-Länge berechnen und die drei unterschiedlichen User-Gruppen anhand der Tweet-Länge vergleichen
# * **Übung 07:**

# # Übung 05

# ## Die Daten laden

# +
# zum Laden der Tabellen brauchen wir ein paar Bibliotheken:

# zum Speichern von Tabellen
import pandas as pd 

# zum Laden von Daten aus dem Internet
import requests 
import io
# -

# Den Datensatz der "normalen" User müssen wir erst herunterladen, entpacken und können ihn dann als Pandas DataFrame laden:
# * gehe auf die Seite http://help.sentiment140.com/for-students/
# * lade das Archiv ```trainingtestdata``` mit dem Datensatz herunter
# * entpacke das Archiv
# * lies die Datei ```training.1600000.processed.noemoticon.csv``` wie folgt ein:  

# unter diesem Pfad sollte der Datensatz gespeichert sein
pfad_normal = 'daten/training.1600000.processed.noemoticon.csv'
tweets_normal_raw = pd.read_csv(pfad_normal, encoding="ISO-8859-1")
tweets_normal_raw.head()

# **HINWEIS:** durch das Argument ```encoding``` geben wir explizit an, welchen Standard für die Zeichenkodierung des Textes Pandas verwenden soll, um den Text in dem Datensatz zu interpretieren. Das ist insbesondre wichtig, da Tweets sehr viele Sonderzeichen enthalten können, die nicht im normalen Alphabet abgebildet sind.  
# Der hier verwendete Standard [ISO 8859-1](https://de.wikipedia.org/wiki/ISO_8859-1) ermöglicht es, den Tweet-Datensatz ohne Fehler einzulesen. Würden wir das encoding nicht explizit angeben, würde Pandas per Default [UTF-8](https://de.wikipedia.org/wiki/UTF-8) verwenden und einen ```UnicodeDecodeError``` produzieren.  
#
# Beim Einlesen fällt auf, dass der Datensatz keinen "Header" (also eine Kopfzeile mit den Namen der Spalten) enthält. Pandas verwendet automatisch die erste Zeile als Header, was in diesem Fall zu einem Falschen Ergebnis führt, da die erste Zeile bereits Daten enthält. Das können wir umgehen, indem wir beim Laden der Daten einen Header explizit angeben:

tweets_normal_raw = pd.read_csv(pfad_normal, \
                            names=['sentiment','ID','Date','Query','User','Tweet'],\
                            encoding="ISO-8859-1")
tweets_normal_raw.head()

# Die verschiedenen Argumente, um die Funktionsweise der Funktion ```read_csv()``` von Pandas zu modifizieren finden sich in der [Dokumentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html) der Funktion. Falls eine Funktion nicht direkt das tut, was du erwartest, ist es oftmals eine gute Idee, als ersten Schritt in der Dokumentation nachzusehen.  
#
# Die anderen beiden Datensätze können wir direkt aus dem Internet herunterladen:

# +
# die URL unter der der Trump-Tweet-Datensatz zu finden ist
url_trump = "https://github.com/mkearney/trumptweets/raw/master/data/trumptweets-1515775693.tweets.csv"

# lies die Information von der in der URL hinterlegten Website aus
# diesmal geben wir das encoding schon an dieser Stelle an
antwort_trump = requests.get(url_trump).content
inhalt_trump = io.StringIO(antwort_trump.decode('ISO-8859-1'))

# lade die Daten in ein DataFrame und zeige die ersten paar Zeilen an
tweets_trump_raw = pd.read_csv(inhalt_trump)
tweets_trump_raw.head()

# +
# die URL unter der der Troll-Tweet-Datensatz zu finden ist
url_troll = 'https://github.com/fivethirtyeight/russian-troll-tweets/raw/master/IRAhandle_tweets_1.csv'

# lies die Information von der in der URL hinterlegten Website aus
# auch hier geben wir das encoding schon an dieser Stelle an
antwort_troll = requests.get(url_troll).content
inhalt_troll = io.StringIO(antwort_troll.decode('ISO-8859-1'))

# lade die Daten in ein DataFrame und zeige die ersten paar Zeilen an
tweets_troll_raw = pd.read_csv(inhalt_troll)
tweets_troll_raw.head()
# -

# ## Die Daten erkunden
# Zu Beginn möchten wir ein paar grundlegende Dinge über den Datensatz herausfinden: z.B. die enthaltene Information, die Anzahl der Tweets in jedem der Datensätze und die Anzahl der User.  
# Die Namen der Spalten eines DataFrame lassen sich über die Variable ```columns``` des DataFrames ansehen:

# eine Liste der im DataFrame "tweets_troll_raw" enthaltenen Spalten
tweets_troll_raw.columns

# In einem DataFrame können wir auf eine bestimmte Spalte mit ihrem Namen zugreifen:

# zeigt die Spalte mit dem Namen "author" im DataFrame 
# "tweets_troll_raw" an
tweets_troll_raw.author

# Mit der Funktion ```unique()``` lassen sich die einzigartigen Einträge in einer Spalte herausfinden:

# eine Liste der User-Namen, wobei jeder User genau einmal
# vorkommt
tweets_troll_raw.author.unique()

print('Anzahl Troll Tweets: {}'.format(len(tweets_troll_raw)))
unique_users = len(tweets_troll_raw.author.unique())
print('Anzahl Troll User: {}'.format(unique_users))

# ### Aufgaben
# * Wie heißen die Spalten in den drei Datensätzen und welche Informationen enthalten sie?
# * Sieh dir ein paar Tweets aus den jeweiligen Datensätzen an, indem du auf die Spalte mit dem Tweet-Text und den Index des Tweets zugreifst.
# * Wieviele Tweets und User sind in dem Datensatz mit "normalen" Tweets, wieviele im Datensatz zu den Trump Tweets?
# * Was ist die mittlere Anzahl an Tweets je User (für alle drei Datensätze)?
# * **(Optional):** Welcher User im Troll-Datensatz hat die meisten Tweets produziert und wieviele Tweets sind das?
# * **(Optional):** der Troll-Datensatz listet auch die Tweet-Sprache. Wieviele Sprachen sind im Datensatz vertreten und wieviele Tweets gibt es je Sprache?

# ### Lösung

# Trolle
unique_users = len(tweets_troll_raw.author.unique())
print('number of unique normal users: {}'.format(unique_users))
print('average number of tweets per user: {:1.2f}'.format(len(tweets_troll_raw) / unique_users))

# Trump
unique_users = len(tweets_trump_raw.screen_name.unique())
print(tweets_trump_raw.screen_name.unique())
print('number of unique normal users: {}'.format(unique_users))
print('average number of tweets per user: {:1.2f}'.format(len(tweets_trump_raw) / unique_users))

# ## Datensätze bereinigen

# Offensichtlich gibt es im Datensatz der Trump-Tweets nicht nur Tweets von Trump selbst [@realDonaldTrump](https://twitter.com/realdonaldtrump) sondern auch noch von vier anderen Usern. Im Folgenden wollen wir die Datensätze etwas aufräumen, um sie leichter nutzbar zu machen.

# +
# originale Anzahl Tweets
print('Anzahl der Tweets vor Bereinigung: {}'.format(len(tweets_trump_raw)))

# entferne alle Tweets von Usern, die nicht "realDonaldTrump"
# sind aus dem Datensatz.
mask_realDonald = tweets_trump_raw['screen_name'] == 'realDonaldTrump'
tweets_trump_filtered = tweets_trump_raw[mask_realDonald]

# wir sehen: es sind gar nicht so viele Tweets verschwunden.
print('Anzahl der Tweets nach Bereinigung: {}'.format(len(tweets_trump_filtered)))
# -

# Es fällt auf, dass die Spalten in den drei Datensätzen unterschiedlich heißen. Die Spalte mit dem User, der einen Tweet erstellt hat, heißt z.B. ```User``` (normale Tweets), ```screen_name``` (Trump Tweets) und ```author``` (Troll Tweets), obwohl sie die gleiche Information - nämlich den Username - enthält. Um die Daten in Zukunft einfacher handhaben zu können, wollen wir sicherstellen, dass alle Spalten in allen Datensätzen gleich heißen. Außerdem löschen wir alle Spalten mit Informationen, die nicht relevant für uns sind. Dafür erstellen wir basierend auf den DataFrame mit den (gefilterten) Rohdaten ein neues DataFrame, das nur die Spalten (mit den richtigen Namen) enthält, die wir brauchen:

# neues, gesäubertes DataFrame
tweets_trump = pd.DataFrame({'Date':tweets_trump_filtered['created_at'],\
                             'User':tweets_trump_filtered['screen_name'],\
                             'Tweet':tweets_trump_filtered['text']})

# ### Aufgaben
# Generell interessieren wir uns für die Spalten, in denen der Username, das Tweet-Datum und der Tweet-Text enthalten sind. Bei den Troll Tweets möchten wir außerdem die Spalten mit Informationen über die Sprache, die Anzahl der Follower und die Anzahl der gefolgten Accounts.  
#
# * Bereinige auch die Tweets von den normalen Usern, indem du ein neues DataFrame mit den Spalten ```[Date, User, Text]``` basierend auf den Rohdaten erstellst.
# * Bereinige die Troll Twees wie die Trump Tweets, behalte zusätzlich auch die Spalten, die die Informationen über die Tweet-Sprache, die Anzahl der Follower und die Anzahl der gefolgten Accounts enthalten.
# * **(Optional):** Das Format der Spalte "Date" ist noch in allen drei Datensätzen unterschiedlich. Versuche, alle drei unterschiedlichen Zeit- und Datumsangaben in einen String mit dem Format ```YYYY-MM-DD hh:mm:ss``` (Year-Month-Day hour:minute:second) zu überführen. Noch eleganter ist die Benutzung eines [datetime](https://docs.python.org/2/library/datetime.html) Objektes, um Datum und Uhrzeit zu speichern. 

# ### Lösung

# +
tweets_normal = pd.DataFrame({'Date':tweets_normal_raw['Date'],\
                              'User':tweets_normal_raw['User'],\
                              'Tweet':tweets_normal_raw['Tweet']})

tweets_normal.head()
# -

tweets_troll = pd.DataFrame({'Date':tweets_troll_raw['publish_date'],\
                             'User':tweets_troll_raw['author'],\
                             'Tweet':tweets_troll_raw['content'],\
                             'Language':tweets_troll_raw['language'],\
                             'Follower':tweets_troll_raw['followers'],\
                             'Following':tweets_troll_raw['following']})
tweets_troll.head()

# ## Eine neue Spalte mit einer (berechneten) Observablen hinzufügen
# Im Folgenden interessieren wir uns für die Länge der einzelnen Tweets. Diese können wir berechnen und in den DataFrames speichern:

# +
length = []
for tweet in tweets_trump['Tweet']:
    length.append(len(tweet))

tweets_trump['Length'] = length
tweets_trump.head()


# -

# ### Aufgaben
# * Erstelle eine Funktion, die für ein gegebenes DataFrame die Tweet-Länge berechnet und eine neue Spalte mit der Tweet-Länge zum DataFrame hinzufügt. Wende diese Funktion auf alle drei Tweet-Datensätze an.
# * **Optional**: Erstelle eine zweite Funktion, die die Anzahl der Worte je Tweet berechnet und ebenfalls als zusätzliche Spalte dem DataFrame hinzufügt.
# * Speichere alle drei Dataframes als ```.csv``` Datei, indem du die Funktion [to_csv()](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html) des Dataframes benutzt.

# ### Lösung

def calculate_length(df):
    length = []
    for tweet in df['Tweet']:
        length.append(len(tweet))

    df['tweet_length'] = length


calculate_length(tweets_normal)
calculate_length(tweets_trump)
calculate_length(tweets_troll)

tweets_normal.to_csv('tweets_normal.csv')

tweets_trump.to_csv('tweets_trump.csv', encoding='ISO-8859-1')

tweets_troll.to_csv('tweets_troll.csv')

# # Übung 06
# Zur Vorbereitung laden wir die bereinigten Datensätze, die wir in der letzten Übung erstellt haben:

tweets_trump = pd.read_csv('daten/tweets_trump.csv', encoding='ISO-8859-1')
tweets_trump.head()

tweets_normal = pd.read_csv('daten/tweets_normal.csv', encoding='ISO-8859-1')

for i, d in enumerate(tweets_trump['Date']):
    if d == '140':
        print(i)

from datetime import datetime, timedelta
tweets_trump['Date'] = [datetime.strptime(date, '%Y-%m-%d %H:%M:%S') for date in tweets_trump['Date']]

tweets_trump.sort_values('Date',inplace=True)

tweets_trump['timedelta'] = [(date - tweets_trump.loc[0]['Date']).total_seconds()/(60*60) for date in tweets_trump['Date']]

tweets_trump['timedelta'] = tweets_trump['timedelta'] - tweets_trump['timedelta'].min()

tweets_trump.head()

tweets_trump.reset_index(drop=True, inplace=True)

# +
gaps = [0]
for i in range(1,len(tweets_trump)):
    gap = tweets_trump.loc[i]['timedelta'] - tweets_trump.loc[i - 1]['timedelta']
    gaps.append(gap)
    
tweets_trump['time_gap'] = gaps
# -

plt.hist(tweets_trump['time_gap'].dropna(),range=[0,24],bins=50)
plt.axvline(x=8, color='red')

tweets_trump['timedelta'].max() / len(tweets_trump)

tweets_trump['time_gap'].median()

tweets_trump['year'] = [date.year for date in tweets_trump['Date']]

tweets_trump.head()

for i,tweet in enumerate(tweets_trump['Tweet']):
    if not type(tweet) == type('str'):
        print(i)

tweets_trump.drop(index=32807, inplace=True)

tweets_trump['link_number'] = [tweet.count('http') for tweet in tweets_trump['Tweet']]
tweets_troll['link_number'] = [tweet.count('http') for tweet in tweets_troll['Tweet']]
tweets_normal['link_number'] = [tweet.count('http') for tweet in tweets_normal['Tweet']]

tweets_troll['link_number'].sum() / len(tweets_troll)

tweets_trump['link_number'].sum() / len(tweets_trump)

tweets_normal['link_number'].sum() / len(tweets_normal)

# +
mean = []
median = []
std = []

for year in range(2009, 2018):
    print(year)
    maske_year = tweets_trump['year'] == year
    tweets_trump_year = tweets_trump[maske_year]
    mean.append(tweets_trump_year['time_gap'].mean())
    median.append(tweets_trump_year['time_gap'].median())
    std.append(tweets_trump_year['tweet_length'].std())
# -

plt.plot(range(2009,2018), mean, '-o')
plt.plot(range(2009,2018), median, '-o')
plt.plot(range(2009,2018), std, '-o')

# +
mean_l = []
mean_w = []
median_l = []
std_l = []

for year in range(2009, 2018):
    print(year)
    maske_year = tweets_trump['year'] == year
    tweets_trump_year = tweets_trump[maske_year]
    mean_l.append(tweets_trump_year['tweet_length'].mean())
    mean_w.append(tweets_trump_year['word_number'].mean())
    median_l.append(tweets_trump_year['tweet_length'].median())
    std_l.append(tweets_trump_year['tweet_length'].std())
# -

plt.plot(range(2009,2018), mean_l, '-o')
plt.plot(range(2009,2018), median_l, '-o')
plt.plot(range(2009,2018), std_l, '-o')

plt.plot(range(2009,2018), mean, '-o')
plt.plot(range(2009,2018), mean_l, '-o')
plt.plot(range(2009,2018), mean_w, '-o')

import matplotlib.pyplot as plt
plt.plot(tweets_trump['timedelta'], tweets_trump['tweet_length'])
plt.xlabel('time / years')
plt.ylabel('tweet length / characters')
#plt.xlim(7.5,8)

tweets_troll = pd.read_csv('daten/tweets_troll.csv', encoding='ISO-8859-1')
tweets_troll['Date'] = [datetime.strptime(date, '%Y-%m-%d %H:%M:%S') for date in tweets_troll['Date']]

tweets_troll['timedelta'] = [(date - tweets_troll.loc[0]['Date']).total_seconds()/(60*60*24*365) for date in tweets_troll['Date']]

tweets_troll['timedelta'] = tweets_troll['timedelta'] - tweets_troll['timedelta'].min()

tweets_troll.sort_values('Date',inplace=True)

plt.plot(tweets_troll['timedelta'], tweets_troll['tweet_length'])
plt.xlabel('time / years')
plt.ylabel('tweet length / characters')
#plt.xlim(7.5,8)

plt.scatter(tweets_normal['tweet_length'], tweets_normal['word_number'])

# ## Datensätze erkunden
# Eine einfache Möglichkeit, einen Datensatz, der Zahlen enthält (visuell) zu erkunden, ist das Histogram. Um Visualisierungen zu erstellen, benutzen wir die Bibliothek ```matplotlib.pyplot``` und die Funktion ```hist()```:

# +
# importiere die Bibliothek matplotlib.pyplot unter dem Kürzel "plt"
import matplotlib.pyplot as plt

# mit diesem Kommando bringen wir Matplotlib dazu, Grafiken
# direkt im Jupyter-Notebook anzuzeigen
# %matplotlib inline

# ein einfaches Histogram der Follower der Trolle
plt.hist(tweets_troll['Follower']);
# -

# Um die im Histogram enthaltene Information etwas besser lesbar zu machen, können wir mit ```plt.xlabel()```, ```plt.ylabel()``` und ```plt.title()``` Achsenbeschriftungen und einen Titel hinzufügen. Außerdem können wir der in der Abbildung dargestellten Information ein ```label``` geben, das später in einer Legende mit Hilfe der Funktion ```plt.legend()```  angezeigt wird.

# +
plt.hist(tweets_troll['Follower'], label='Follower der Trolle')

# wir möchten Achsenbeschriftungen und einen
# Titel für den Plot anzeigen.
plt.title('Follower der Trolle')
plt.xlabel('Anzahl Follower')
plt.ylabel('Trolle')

# Außerdem geben wir dem Plot eine Legende, indem wir zuerst in
# der hist()-Funktion ein "Label" definieren und dann über
# die legend()-Funktion einen Legene erzeugen, die alle zuvor
# definierten Labels anzeigt.
plt.legend();
# -

# ### Aufgaben
# Mit dem Argument ```range=[start, stop]``` kann das Histogram nur in einem gewissen Intervall zwischen ```start``` und ```stop``` angezeigt werden. Mit dem Argument ```bins``` (nimmt als Argument einen ganze Zahl an) kann die Anzahl der Bins in dem Histogram verändert werden.
# * Mach dir die Wirkung von verschiedenen Werten für das Intervall in ```range``` klar, indem du verschiedene Werte ausprobierst.
# * Mach dir die Wirking von verschiedenen Werten für ```bins``` klar, indem du verschiedene Werte ausprobierst.
# * Was für Probleme gibt es mit dieser Visualisierung? Welche Werte eignen sich, um die Anzahl der Follower sinnvoll zu veranschaulichen? 
# * Visualisiere die Anzahl der gefolgten Accounts (```'Following'```) im selben Histogram. HINWEIS: Dafür kannst du einfach in der selben Code-Zelle zwei mal hintereinander den Befehl ```plt.hist()``` mit unterschiedlichen Daten aufrufen. Achte darauf, dass das angezeigte Interval und die Bins der beiden Histogramme vergleichbar sind. Was für Aussagen über die Troll-Accounts kannst du aus dieser Visualisierung ableiten?

# +
plt.hist(tweets_troll['Follower'], label='Follower')
plt.hist(tweets_troll['Following'], label='Following')

# wir möchten Achsenbeschriftungen und einen
# Titel für den Plot anzeigen.
plt.title('Follower der Trolle')
plt.xlabel('Anzahl Follower')
plt.ylabel('Trolle')

# Außerdem geben wir dem Plot eine Legende, indem wir zuerst in
# der hist()-Funktion ein "Label" definieren und dann über
# die legend()-Funktion einen Legene erzeugen, die alle zuvor
# definierten Labels anzeigt.
plt.legend();
# -

# ### More questions
# * maximum number of tweets per user?
# * number of words per user?
# * number of characters per user?
# * number of unique words in all tweets
# * number of unique words in Trump tweets
# * stdev of tweets per user?

# ## Einfache Visualisierung mit Histogrammen

# eine einfache Visualisierung der Tweet-Länge mit einem Histogram
plt.hist(tweets_normal['tweet_length']);

# +
# Tweets haben eine maximale Zeichenzahl von 140, deswegen
# macht es sinn, die Bins des Histograms auf diesen Bereich
# einzuschränken. Das geht mit dem "range"-Argument der Funktion
# "hist()".
# Außerdem geben wir dem Plot eine Legende, indem wir zuerst in
# der hist()-Funktion ein "Label" definieren und dann über
# die legend()-Funktion einen Legene erzeugen, die alle zuvor
# definierten Labels anzeigt.
plt.hist(tweets_normal['tweet_length'],range=[0,140], label='normale Tweets')
plt.legend()

# außerdem möchten wir Achsenbeschriftungen und einen
# Titel für den Plot anzeigen. Das funktioniert mit den 
# "title()", "xlabel()" und "ylabel()"-Funktionen
plt.title('Tweet-Längen der normalen User-Gruppe')
plt.xlabel('Buchstaben im Tweet')
plt.ylabel('Anzahl Tweets');
# -

# ### Aufgaben
# HINWEIS: die Bedeutung der verschiedenen Argumente der ```hist()``` Funktion und ihre Möglichen Werte liest du am besten in der [Dokumentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.hist.html) nach!
# * Variiere den Wert des Arguments "bins" der hist()-Funktion. Was passiert, wenn dieser Wert gleich 5, 10, 20, 50, 100 oder 140 ist? Was ist die beste Wahl für dieses Histogram und warum?
# * Visualisiere die Tweet-Längen von Trump-Tweets und Troll-Tweets per Histogram im selben Plot wie die der normalen Tweets. HINWEIS: Du kannst einfach durch wiederholtes Aufrufen der "plt.hist()"-Funktion in der selben Code-Zeile neue Dinge im selben Plot anzeigen.
# * Welche Probleme treten bei dieser naiven Visualisierung auf? Versuche, die Information besser (und vor allem unabhängig von der Anzahl der Tweets in jedem der drei Datensätze) darzustellen. Dabei hilft dir das "density=True" Argument der hist()-Funktion. 
# * Was hat es mit diesem Argument auf sich? Wie wird die Darstellung des Histogramms verändert und was bedeutet das? Du kannst auch das Argument "alpha" der hist() Funktion ausprobieren: dieses Argument setzt den Transparenzwert des Histograms fest und könnte hilfreich sein, um mehrere Histogramme übereinander besser erkennbar anzuzeigen.
# * **Optional:** die Argumente ```rwidth``` und ```color``` lassen dich die Breite der Bins sowie deren Farbe verändern. Währe Werte so, dass die in dem Histogram enthaltene Information möglichst klar und einfach erkennbar dargestellt wird. Warum hast du die Werte so gewählt? Worauf sollte man vor allem bei der Auswahl von Farben in Plots achten? 

# +
plt.hist(tweets_normal['tweet_length'],bins=20,density=True,\
         label='Normale tweets', rwidth=0.8, range=[0,140], alpha=0.5)
plt.hist(tweets_trump['tweet_length'],bins=20,density=True,\
         label='Trump tweets', rwidth=0.8, range=[0,140], alpha=0.5)
plt.hist(tweets_troll['tweet_length'],bins=20,density=True,\
         label='Troll tweets', rwidth=0.8, range=[0,140], alpha=0.5)

plt.xlabel('Buchstaben im Tweet')
plt.ylabel('Anzahl Tweets')

plt.legend()
# -

# # Tutorium 2

# +
# play with the bins: 10, 20, 100, 150, 300 (play with rwidth)
plt.hist(tweet_sentiment_short['tweet_length'],bins=150,\
         label='normal tweets', rwidth=1, range=[0,150], alpha=0.5)

plt.xlabel('Buchstaben im Tweet')
plt.ylabel('Anzahl Tweets')

plt.plot([69,69],[0,30000],color='red', label='median')
plt.plot([74,74],[0,30000],color='green', label='mean')

plt.legend()
# -

tweet_sentiment_short['tweet_length'].describe()

tweet_trump.head()

tweet_trump.loc[-1] = {'Date':'0000-00-00-00:00:00+00:00','Time':'00:00:00',\
                   'Tweet':'a'*int(1e4),'Tweetid':111111111111111111,\
                    'tweet_length':1e4}

print(tweet_trump['tweet_length'].mean())
print(tweet_trump['tweet_length'].median())

# +
# play with the bins: 10, 20, 100, 150, 300 (play with rwidth)
plt.hist(range(200),bins=200,\
         label='Trump tweets', rwidth=1, range=[0,200], alpha=0.5)

plt.xlabel('Buchstaben im Tweet')
plt.ylabel('Anzahl Tweets')

plt.plot([137,137],[0,30000],color='red', label='median')
plt.plot([158,158],[0,30000],color='green', label='mean')

plt.legend()
# -

# What is the typical length of an English sentence?

# +
# tweet length
length = []
for tweet in tweets_troll['content']:
    length.append(len(tweet))
    
tweets_troll['tweet_length'] = length
# -

print(len(tweets_troll))
print(len(tweet_sentiment))

# +
# play with the bins: 10, 20, 100, 150, 300 (play with rwidth)
plt.hist(tweets_troll['tweet_length'],bins=150,density=True,\
         label='troll tweets', rwidth=1, range=[0,150], alpha=0.5)

plt.hist(tweet_sentiment_short['tweet_length'],bins=150,density=True,\
         label='normal tweets', rwidth=1, range=[0,150], alpha=0.5)

plt.xlabel('Buchstaben im Tweet')
plt.ylabel('Anzahl Tweets')

plt.legend()

# +
troll_tweets = []
troll_path = 'daten/allgemein/russian-troll-tweets/'

for i in range(1, 5):
    troll_name = 'IRAhandle_tweets_{}.csv'.format(i)
    troll_tweets.append(pd.read_csv(join(troll_path, troll_name)))
# -

for df in troll_tweets:

    length = []
    for tweet in df['content']:
        length.append(len(tweet))

    df['tweet_length'] = length

# +
# play with the bins: 10, 20, 100, 150, 300 (play with rwidth)

for i,df in enumerate(troll_tweets):
    plt.hist(df['tweet_length'],bins=150,density=True,\
             label='troll tweets {}'.format(i+1), rwidth=1, range=[0,150], alpha=0.5)
    

plt.hist(tweet_sentiment_short['tweet_length'],bins=150,density=True,color='k',\
             label='sentiment tweets {}'.format(i+1), rwidth=1, range=[0,150], alpha=0.5)

plt.xlabel('Zeichen im Tweet')
plt.ylabel('Anzahl Tweets')

plt.legend()
# -

# ### Fragen 
# * was hat es mit den Spitzen zwischen 20 und 30 Zeichen auf sich?
# * wie lassen sich die verschiedenen Troll-Tweet Datensätze charakterisieren? Was unterscheidet sie? (Datum, User, ...)?

# +
# tweet length
length = []
for tweet in tweets_troll2['content']:
    length.append(len(tweet))
    
tweets_troll2['tweet_length'] = length
# -

# ## Hashtags

from os.path import join
import pandas as pd
path = "/home/jana/DaLeLe/uebungen/daten-lesen-lernen-repo/daten"
tweets_trump = pd.read_csv(join(path, 'tweets_trump.csv'))
tweets_troll = pd.read_csv(join(path, 'tweets_troll_subset.csv'))
tweets_normal = pd.read_csv(join(path, 'tweets_normal_subset.csv'), encoding='ISO-8859-1')

tweets_trump.head()


def isHashTag(word):
    if '#' in word:
        return True
    return False


def isWord(word):
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',\
               'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    uppercase_alphabet = [letter.title() for letter in alphabet]
    alphabet.extend(uppercase_alphabet)
    
    for letter in word:
        if letter not in alphabet:
            return False
        
    return True


# +
# all words used by Trump
hashtag_number_trump = []
hashtags_trump = []

for tweet in tweets_trump['Tweet']:
    words = tweet.split(' ')
    hashtags = [word for word in words if isHashTag(word)]
    hashtag_number_trump.append(len(hashtags))
    hashtags_trump.extend(hashtags)
    
tweets_trump['hashtag_number'] = hashtag_number_trump
# -

tweets_trump['hashtag_number'].mean()

# filter for english language
language_mask = tweets_troll['Language'] == 'English'
tweets_troll = tweets_troll[language_mask]

# +
# all words used by trolls
#words_troll = []
hashtag_number_troll = []
hashtags_troll = []

for tweet in tweets_troll['Tweet']:
    words = tweet.split(' ')
    hashtags = [word for word in words if isHashTag(word)]
    hashtag_number_troll.append(len(hashtags))
    hashtags_troll.extend(hashtags)
    
tweets_troll['hashtag_number'] = hashtag_number_troll

# +
# all words used by normal people
hashtag_number_normal = []
hashtags_normal = []

for tweet in tweets_normal['Tweet']:
    words = tweet.split(' ')
    hashtags = [word for word in words if isHashTag(word)]
    hashtag_number_normal.append(len(hashtags))
    hashtags_normal.extend(hashtags)
    
tweets_normal['hashtag_number'] = hashtag_number_normal
# -

hashtags_trump[0:5]

hashtags_troll[0:5]

hashtags_normal[0:5]

# ## Average number of hashtags of different usergroups

for df, name in zip([tweets_trump, tweets_troll, tweets_normal], ['trump', 'troll', 'normal']):
    print('The mean number of hashtags / tweet for {} user(s) is {:1.4f}, the std is {:1.4f}'\
          .format(name, df['hashtag_number'].mean(), df['hashtag_number'].std()))

# ## How many different hashtags?

hashtags_trump = list(set(hashtags_trump))
hashtags_trump.sort()
print('number of unique hashtags: {}'.format(len(hashtags_trump)))

hashtags_troll = list(set(hashtags_troll))
hashtags_troll.sort()
print('number of unique hashtags: {}'.format(len(hashtags_troll)))

hashtags_normal = list(set(hashtags_normal))
hashtags_normal.sort()
print('number of unique hashtags: {}'.format(len(hashtags_normal)))

# trump hashtag length
hashtags_trump = pd.DataFrame({'tag':hashtags_trump,'length':[len(tag) for tag in hashtags_trump],\
                        'source':['troll']*len(hashtags_trump)})
hashtags_trump.head()

# troll hashtag length
hashtags_troll = pd.DataFrame({'tag':hashtags_troll,'length':[len(tag) for tag in hashtags_troll],\
                        'source':['troll']*len(hashtags_troll)})
hashtags_troll.head()

# normal hashtag length
hashtags_normal = pd.DataFrame({'tag':hashtags_normal,'length':[len(tag) for tag in hashtags_normal],\
                        'source':['sentiment']*len(hashtags_normal)})
hashtags_normal.head()

import matplotlib.pyplot as plt
plt.hist([hashtags_trump['length'], hashtags_troll['length'], hashtags_normal['length']],\
         rwidth=0.8, bins=20, range=[1,40], density=True,\
         label=['trump','troll','normal'])
plt.xlabel('hashtag length')
plt.ylabel('length count')
plt.legend()

hashtags_troll[hashtags_troll['length'] > 30]

# ## Unique words: Trump vs. other
# English has around 180k unique words ([source](https://en.oxforddictionaries.com/explore/how-many-words-are-there-in-the-english-language/))

# +
#words_trump_filtered = [word.lower() for word in words_trump if not isHashTag(word) and isWord(word.lower())]
import re

# all words used by Trump
words_trump = []
word_length_trump = []

for tweet in tweets_trump['Tweet']:
    # split the Tweet into single words along spaces
    words = tweet.split(' ')
    # remove all hashtags and ensure lowercase spelling
    words = [word.lower() for word in words if not isHashTag(word)]
    # remove all symbols which aren't the characters from a-z
    words = [re.sub(r'[^a-z]', '', word) for word in words]
    # remove all empty strings which might have resulted from above step
    words = [word for word in words if word != '']
    # add the new filtered words to the global list of words
    words_trump.extend(words)
    
    # calculate word length for every (filtered) word from the Tweet
    word_length = [len(word) for word in words]
    # add the new list of word-lengths to the global list
    word_length_trump.extend(word_length)

words_trump_df = pd.DataFrame({'word':words_trump, 'length':word_length_trump})
del(words_trump)
del(word_length_trump)
words_trump_df.head()
# -

len(words_trump_df.word.unique())

# +
# all words used by the trolls
words_troll = []
word_length_troll = []

for tweet in tweets_troll['Tweet']:
    # split the Tweet into single words along spaces
    words = tweet.split(' ')
    # remove all hashtags and ensure lowercase spelling
    words = [word.lower() for word in words if not isHashTag(word)]
    # remove all symbols which aren't the characters from a-z
    words = [re.sub(r'[^a-z]', '', word) for word in words]
    # remove all empty strings which might have resulted from above step
    words = [word for word in words if word != '']
    # add the new filtered words to the global list of words
    words_troll.extend(words)
    
    # calculate word length for every (filtered) word from the Tweet
    word_length = [len(word) for word in words]
    # add the new list of word-lengths to the global list
    word_length_troll.extend(word_length)

words_troll_df = pd.DataFrame({'word':words_troll, 'length':word_length_troll})
del(words_troll)
del(word_length_troll)
words_troll_df.head()
# -

len(words_troll_df.word.unique())

# +
# all words used by the trolls
words_normal = []
word_length_normal = []

for tweet in tweets_normal['Tweet']:
    # split the Tweet into single words along spaces
    words = tweet.split(' ')
    # remove all hashtags and ensure lowercase spelling
    words = [word.lower() for word in words if not isHashTag(word)]
    # remove all symbols which aren't the characters from a-z
    words = [re.sub(r'[^a-z]', '', word) for word in words]
    # remove all empty strings which might have resulted from above step
    words = [word for word in words if word != '']
    # add the new filtered words to the global list of words
    words_normal.extend(words)
    
    # calculate word length for every (filtered) word from the Tweet
    word_length = [len(word) for word in words]
    # add the new list of word-lengths to the global list
    word_length_normal.extend(word_length)

words_normal_df = pd.DataFrame({'word':words_normal, 'length':word_length_normal})
del(words_normal)
del(word_length_normal)
words_normal_df.head()
# -

len(words_normal_df.word.unique())

# [Checking if word is in dictionary with pyenchant](https://stackoverflow.com/questions/3788870/how-to-check-if-a-word-is-an-english-word-with-python)

import sys
!{sys.executable} -m pip install pyenchant

import enchant
dictionary = enchant.Dict("en_US")
dictionary.check("Helo")

dict_mask = words_trump_df['word'].apply(dictionary.check)
words_trump_df = words_trump_df[dict_mask]

dict_mask = words_troll_df['word'].apply(dictionary.check)
words_troll_df = words_troll_df[dict_mask]

dict_mask = words_normal_df['word'].apply(dictionary.check)
words_normal_df = words_normal_df[dict_mask]

N_trump = len(words_trump_df.word.unique())
N_troll = len(words_troll_df.word.unique())
N_normal = len(words_normal_df.word.unique())

plt.bar(['Trump', 'trolls', 'normal users'],[N_trump, N_troll, N_normal])
plt.ylabel('# of unique English words')

# misleading visualization
plt.bar(['Trump', 'trolls', 'normal users'],[N_trump, N_troll, N_normal])
plt.ylim(12000,15000)
plt.ylabel('# of unique English words')

# ## No better word than stupid
# [Source](https://www.washingtonpost.com/video/national/trump-i-have-the-best-words/2017/04/05/53a9ae4a-19fd-11e7-8598-9a99da559f9e_video.html?noredirect=on&utm_term=.d27db69842c9)

words_trump_df.word.str.count("stupid").sum()

# Best words really short! See [comparison](http://www.ravi.io/language-word-lengths)

words_trump_df['length'].mean()

words_troll_df['length'].mean()

words_normal_df['length'].mean()

# aha!

plt.hist(words_trump_df['length'],bins=18);

# ## Reproducing Zipf's law
# [Source](https://en.wikipedia.org/wiki/Zipf%27s_law)

# why is this taking so long?
frequency = []
for word in words_trump_df.word.unique():
    word_count = 0
    for temp_word in words_trump_df['word']:
        if word == temp_word:
            word_count += 1
    #f = words_trump_df.word.str.count(word).sum()
    frequency.append(word_count)

# frequency_df = pd.DataFrame({'word':words_trump_df.word.unique(), 'frequency':frequency})
# frequency_df.to_csv('daten/wortfrequenzen.csv')

# frequency_df.sort_values(by='frequency', ascending=False, inplace=True)
# frequency_df.head(20)

f_df =  pd.DataFrame({'word':words_trump_df.word.unique()[0:10], 'frequency':frequency})
f_df.sort_values(by='frequency', ascending=False, inplace=True)
f_df.head(20)

#plt.xscale('log')
plt.hist(frequency_df['frequency'], bins=100, range=[0,1000]);

# * sample different intervals with constant length but random start position in other tweet corpus, calculate number of unique words
# * calculate average number of unique words
# * show distribution of unique words

# * number of tweets over time
# * be aware of different time zones
# * length of hashtags over time
# * tweet length over time

# ## Exkurs: Twitterscraper
# Twitterscraper: https://github.com/taspinar/twitterscraper

# +
import codecs, json

with codecs.open('tweets_DaLeLe_Goe.json', 'r', 'utf-8') as f:
    tweets = json.load(f, encoding='utf-8')

list_tweets = [list(elem.values()) for elem in tweets]
list_columns = list(tweets[0].keys())
df = pd.DataFrame(list_tweets, columns=list_columns)
# -

df.head()
