# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# <a name="daten_beschaffen"></a>**3 Daten säubern**
#
# Im Folgenden wollen wir uns die Texte ein bisschen genauer ansehen und ihre Struktur analysieren. Im besten Fall können wir über die Struktur etwas über den Inhalt des Textes herausfinden. Um die Textstruktur zu verstehen, können wir unser _Domänenwissen_ über Texte benutzen: 
# * Wir wissen, dass Texte in Schriften oft in _Kapiteln_ organisiert sind. Diese Kapitel bestehen aus _Sätzen_, die Sätze wiederum aus _Worten_ und die Worte aus _Zeichen_. 
# * Außerdem gibt es noch Satzzeichen wie Punkte oder Anführungszeichen und Formatierungselemente wie Zeilenumbrüche und Leerzeichen.
# * Schlussendlich gibt es in vielen Sprachen Groß- und Kleinschreibung des selben Wortes, die aber in den allermeisten Fällen nichts über die _Bedeutung_ des Wortes aussagt sondern als Formatierungselement dient.
