# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 05 - Archäologie: Daten finden, einlesen und mit Histogrammen visualisieren
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
#  Mit dieser Übung beginnen wir eine Fallstudie, die sich mit einem Datensatz aus der Archäologie beschäftigt: dem ["Inventory of Crafts and Trade in the Roman East - database of tableware" (ICRATES)](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/downloads.cfm). Außerdem werden wir uns damit beschäftigen, wie wir die gewonnenen Informationen am besten visualisieren können. Die aktuelle Übung gliedert sich in drei Teile:
# * [Daten beschaffen](#daten_beschaffen)
# * [Daten erkunden & bereinigen](#daten_erkunden)
# * [Histogramme](#histogramme)  
#
# Für die folgenden Übungen gibt es kein Lehrvideo. Deswegen wird es in den Jupyter-Notebooks zu den Übungen mehr Erklärungen und Zwischenschritte geben. Darüber hinaus haben ab dieser Woche auch nicht mehr alle Übungen der verschiedenen Übungsgruppen den exakt gleichen Inhalt, da einige Themenbereiche etwas unterschiedliche Werkzeuge brauchen als andere. Das Konzept ist aber überall das gleiche.

# <a name="daten_beschaffen"></a>1. Daten beschaffen
# ---
# **A.** Mache dich mit der Datenquelle und ihrem Aufbau vertraut: Wer hat die Daten gesammelt und in welchem Kontext publiziert? Was für Informationen finden sich im ```ICRATES_CATALOGUE``` und wie sind sie mit den anderen Tabellen wie ```ICRATES_LOCATION``` verknüpft?  
#
# Wir können den Datensatz direkt aus dem Internet herunterladen:

# +
# Bibliotheken zum Laden von Daten aus dem Internet
import requests 
import io

# die URL unter der der Datensatz zu finden ist
pfad_database = "https://archaeologydataservice.ac.uk/catalogue/adsdata/arch-3268-1/dissemination/csv/"

# die Dateinamen der einzelnen Dateien
name_catalogue = "ICRATES_CATALOGUE.csv"

# lies die Information von der in der URL hinterlegten Website aus
# diesmal geben wir das encoding schon an dieser Stelle an
antwort_catalogue = requests.get(pfad_database + name_catalogue).content
inhalt_catalogue = io.StringIO(antwort_catalogue.decode('ISO-8859-1'))

# lade die Daten in ein DataFrame und zeige die ersten paar Zeilen an
import pandas as pd
catalogue = pd.read_csv(inhalt_catalogue)
catalogue.head()
# -

# **B.** Lade auch die anderen zur Datenbasis gehörenden Tabellen wie oben (oder von Hand) herunter.  
# <font color='green'>**HINWEIS:** Du kannst die Tabellen mit der Funktion ```DataFrame.to_csv()``` lokal speichern (Funktionsweise siehe [Dokumentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html). </font>

# [Anfang](#top)

# <a name="daten_erkunden"></a>2. Daten erkunden & bereinigen
# ---
# Zu Beginn möchten wir ein paar grundlegende Dinge über den Datensatz herausfinden: z.B. die enthaltene Information in den Spalten und die Anzahl der Einträge.
# Die Namen der Spalten eines DataFrame lassen sich über die Variable ```columns``` des DataFrames ansehen:

catalogue.to_csv("ICRATES_CATALOGUE.csv")

# eine Liste der im DataFrame "catalogue" enthaltenen Spalten
catalogue.columns

# In einem DataFrame können wir auf eine bestimmte Spalte mit ihrem Namen zugreifen:

# zeigt die Spalte mit dem Namen "Location_specific" im DataFrame 
# "tweets_troll_raw" an
catalogue.Location_specific

# Mit der Funktion ```unique()``` lassen sich die einzigartigen Einträge in einer Spalte herausfinden:

# eine Liste der Fundorte, wobei jeder Fundort genau einmal
# vorkommt
catalogue.Location_specific.unique()

# [Anfang](#top)

# ### Aufgaben
# **A.** Wie heißen die Spalten in den Datensätzen ```ICRATES_LOCATION```, ```ICRATES_DEPOSIT``` und ```ICRATES_STANDARD_FORM``` und welche Informationen enthalten sie?  
# **B.** Sieh dir ein paar Einträge aus dem Katalog an, indem du auf den Index zugreifst.  
# **C.** Wieviele Fundstücke und Fundorte enthält der Katalog? Was ist die mittlere Anzahl an Fundstücken je Fundort?  
# **D.** Die Spalte ```Min_Rim_Diameter_mm``` enthält eine Abschätzung des Umfanges der Tongefäße am oberen Rand. Berechne den mittleren Umfang und den Median des Umfangs. Berechne auch den Modus des Umfangs. Wie kannst du dir das Ergebnis erklären?  
# **E.** Manche Spalten enthalten nur sporadisch Einträge und sind sonst leer. Fehlende Einträge werden im ```DataFrame``` als ```NaN``` ([not a number](https://de.wikipedia.org/wiki/NaN)) dargestellt. Spalten wie ```Gouged``` oder ```Glazed``` enthalten Nullen und Einsen. Wie sind diese zu interpretieren? 
#
# Die Anzahl der ```NaN```-Einträge lässt sich mit der Funktion ```isna()``` herausfinden:

# in der Spalte "Min_Rim_Diameter_mm" des Katalogs z.B.
# gibt es 23137 fehlende Einträge
catalogue.Min_Rim_Diameter_mm.isna().sum()

# **F. (Optional)** Die Nullen in der Spalte ```Min_Rim_Diameter_mm``` sind offensichtlich als "fehlender Eintrag" gemeint. Ersetze alle Nullen in dieser Spalte durch ```NaN```. Speichere die bereinigte Datei mit der Funktion ```catalogue.to_csv()``` ab. Wie verändert sich der Mittelwert, Median und Modus der Spalte?  
# <font color='green'>**HINWEIS:** dafür musst du das "NaN-Objekt" das von der Bibliothek ```NumPy``` bereit gestellt wird benutzen:</font> 

import numpy as np
np.nan

# **G.** Finde für jede Spalte des Katalogs heraus, wieviele (fehlende) Einträge es in der Spalte gibt. Erstelle ein neues ```DataFrame```, das jeweils den Spaltennamen und die Anzahl der Einträge enthält.  
# **H.** Welche Spalten haben keine fehlenden Einträge? In welchen Spalten ist am wenigsten Information enthalten?  
# **I. (Optional)** Erstelle mit Hilfe der Bibliothek ```matplotlib.pyplot``` ein sog. "bar-chart", in dem die Anzahl der fehlenden Einträge je Spalte visualisiert sind.  
# <font color='green'>**HINWEIS:** [Hier](https://pythonspot.com/matplotlib-bar-chart/) findest du ein Beispiel für ein bar-chart und [Hier](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html) ist die Funktion ```bar()``` dokumentiert. </font>

# [Anfang](#top)

# <a name="histogramme"></a>3. Histogramme
# ---
# Im folgenden Abschnitt werden wir damit beginnen, die in den Daten enthaltenen Informationen zu visualisieren. Eine sehr einfache Visualisierung ist das Histogramm, eine graphische Darstellung der Häufigkeit eines Merkmals. Zur Illustration erstellen wir eine kleine Visualisierung der Verteilung von Werten in einer Liste:

# +
# eine Liste mit 30 Ganzzahlen, die z.B. das Alter von Personen in einer
# Gruppe darstellen können
alter = [37, 20, 84,  2, 11, 89, 52, 65, 90, 21, 30, 12, 17, 53, 62, 45, 37,
        3,  6, 64, 85,  6, 95, 73, 68, 55, 86, 83, 91,  1]

# für Visualisierungen benutzen wir die Blbiothek "matplotlib.pyplot"
import matplotlib.pyplot as plt

# Histogramme lassen sich einfach mit der Funktion plt.hist() erstellen
plt.hist(alter);
# -

# Visualisierungen sollten immer mit Achsenbeschriftungen versehen werden. Dafür verwenden wir die Funktionen ```plt.xlabel()``` und ```plt.ylabel```. Außerdem können wir der Abbildung mit ```plt.title()``` einen Titel geben. Mit verschiedenen optionalen Argumenten lässt sich das Verhalten von ```plt.hist()``` anpassen:
# * ```bins``` ändert die Anzahl der Bins
# * ```range``` ändert den dargestellten Bereich auf der x-Achse
# * ```rwidth``` ändert die dargestellte bin-Weite 
# * ```color``` ändert die Farbe
# * ```alpha``` ändert den Alpha-Wert (Durchsichtigkeit) der dargestellten Balken.
#
# Plot-Funktionen haben oftmals sehr viele optionale Argumente (auch "keyword arguments" genannt). Um herauszufinden, wass die jeweiligen Funktionen alles können, lohnt sich ein Blick in die [Dokumentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.hist.html). Außerdem sollte jede Darstellung *immer* mit Achsenbeschriftungen und eventuell einem Titel versehen werden:

plt.hist(alter, color='red', rwidth=0.8, range=[0,100], bins=10, label='alter')
plt.xlabel('Alter')
plt.ylabel('Anzahl')
plt.title('Verteilung des Alters');

# ### Aufgaben
# **A.** Erstelle ein Histogramm der in der Spalte ```Min_Rim_Diameter_mm``` enthaltenen Durchmesser. <font color='green'>**HINWEIS:** Damit das funktioniert, musst du mit der Funktion ```dropna()``` alle ```NaN```-Werte entfernen.</font>  
# **B.** Mache dich mit den unterschiedlichen Argumenten der Funktion ```plt.hist()``` vertraut, indem du verschiedene Werte und Kombinationen für die Daten ausprobierst. Finde Werte, die die Daten deiner Meinung nach gut und verständlich darstellen. Worauf kommt es dabei an? Versieh die Darstellung mit passenden Beschriftungen.  
# **C.** Insbesondere das Argument ```bins``` verändert die Darstellung des Histogrammes sehr stark. Was kann schiefgehen, wenn ```bins``` zu niedrig oder zu hoch gewählt wird? Was ist eine gute Wahl für einen (diesen) Datensatz?  
# **D.** Visualisiere die Durchmesser an der Gefäßbasis (```Min_Base_Diameter_mm```) im selben Histogram. Was für Aussagen über die Gefäße kannst du aus dieser Visualisierung ableiten? <font color='green'>**HINWEIS:** Du kannst einfach in der selben Code-Zelle zwei mal hintereinander die Funktion ```plt.hist()``` (natürlich mit unterschiedlichen Daten) aufrufen, um die Daten übereinandergelegt darzustellen. Du kannst mit der Funktion ```plt.legend()``` eine Legende zur Abbildung hinzufügen. Damit das funktioniert, musst du den einzelen plot-Befehlen vorher ein entsprechendes ```label``` Argument mitgeben.</font> Was fällt dir auf? Worauf musst du achten, damit die in beiden Histogrammen dargestellten Werte vergleichbar sind? Warum ist diese Darstellung eventuell irreführend?  
# **E. (Optional)** Was hat es mit den beiden keyword arguments ```density``` und ```cumulative``` auf sich? Wie verändert sich das Histogram und wie kann die Darstellung dann interpretiert werden?

# [Anfang](#top)
