# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Solutions to Exercise 1: Python as a calculator

# 2.A
print(1 + 1)
print(1 - 1)
print(3 * 4)
print(10 / 5)
print(2 ** 8)

# +
# 2.B
res1 = 10 * 3 + 2
res2 = 10 * (3 + 2)

print(res1)
print(res2)

# +
# 2.C (Optional)
res1 = 10/3
print('Floating point: ')
print(res1)
print(type(res1))

print() # "print()" without argument prints an empty line
print('Integer: ')
res2 = int(10 / 3)
print(res2)
print(type(res2))
# -

# 2.D (Optional)
print(0.1 + 0.2) # the result is not exactly 0.3!

# +
# 2.D Continuation

# We can verify this by testing for numerical
# equality using the `==` operator (we will explore
# this later in depth). A `False` result means for
# a computer, two things being compared are not
# eqaual
0.1 + 0.2 == 0.3
# -

# 3.A
first_name = 'Emmy'
last_name = 'Noether'
text = '''In the spring of 1915, Noether was invited to return to
the University of Göttingen by David Hilbert and Felix Klein. Their
effort to recruit her, however, was blocked by the philologists and
historians among the philosophical faculty: Women, they insisted,
should not become privatdozenten. One faculty member protested:
"What will our soldiers think when they return to the university and
find that they are required to learn at the feet of a woman?" Hilbert
responded with indignation, stating, "I do not see that the sex of
the candidate is an argument against her admission as privatdozent.
After all, we are a university, not a bath house."'''

# 3.B
sentence = first_name + last_name + text
print(sentence)

# 3.C
print('About {} {}: "{}"'.format(first_name, last_name, text))

# +
# 4.A and 4.B

# define a few variables and store integers in them
a = 2
b = 10

# compute the product of these 
product = a * b

# show the product of these integers in a formatted
# string
print('The product of {} and {} is {}.'.format(a, b, product))

# Compute the quotient of b and a
quotient = a / b

# show the quotient of these integers in a formatted
# string
print('The division {} / {} results in {}.'.format(a, b, quotient))

# print also the type of the dividend
print('The dividend {} is of type {}'.format(quotient, type(quotient)))
# -


