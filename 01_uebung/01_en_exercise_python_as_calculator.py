# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Benjamin Säfken (benjamin.saefken@wiwi.uni-goettingen.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Exercise 1
# ### Some tips
# $\rightarrow$ You should take a look at the exercises at home and try to solve as many tasks as you can. In the tutorials you could go on with solving the tasks you could not finish at home and if necessary, ask for help from the tutors. 
#
# $\rightarrow$ You you encounter an error:
# 1. _Read_ and _understand_ the error message. 
# 2. Try to find out the solution to the problem ($\rightarrow$ this may be hard at the beginning, but it is very educational!)
# 3. Search using a search engine for the problem (hint: Stackoverflow) or ask your neighbour!
# 4. Ask the tutors.
#
# $\rightarrow$ Look out for the sign <font color='green'>**HINT**</font>: these are hints that helps you with solving the task and sometimes additional information regarding the task. 
#
# $\rightarrow$ Tasks marked **(Optional)** are for those of you who are extra fast :-).

# ### Python as pocket calculator
# 1. **Starting up**
#   1. Navigate to the Dashboard (preferably by right clicking on the Jupyter logo $\rightarrow$ "Open Link in New Tab")
#   2. Create a new notebook for this exercise: "New" (top right) $\rightarrow$ "Python 3".
#   3. Give the notebook a proper name (e.g.) "exercise01" by clicking on the field at top left that reads "Untitled". 
#   
# 2. **Numbers**
#   1. Write a program that outputs the result of simple arithmetical operations (```print()```). Use each operator (+, - , *, /, \**) at least once. 
#   2. Use more than one arithmetical operator in a single expression. Use brackets ```()``` to change the order of execution of the operators. 
#   3. **(Optional)** Divide an integer with another integer, that does not divide the former. Display the data type of the result with ```type()```. Now round the result to integer (use ```int()```). Display the data type of the result again. 
#   4. **(Optional)** Floating point numbers are not stored on computers *exactly*. Try to find a small example to see it in action. 
# 3. **Strings**
#   1. Think of a famous scientist (from Göttingen, obviously...). Store the first name, last name and a text snippet regarding scientist in three separate variables.
#   2. Add the strings (concatenation) with the name of the scientist and the quotation to form a sentence. Store the sentence in a variable and print it. 
#   3. Format the output with the ```format()``` function to form an understandable and readable output (use whitespaces and punctuation marks).
# 4. **(Optional) Comments in code**
#   1. Write a program of your choice that uses each concept you learnt so far.
#   2. Comment your code in detail, describing what each line does.  
#   
#   <font color='green'>**HINT:** comments in a code cell must start with ```#```. Any text after the ```#``` will not be interpreted as code. The point of comments are to describe what the code is doing.</font>

# +
# Example
# comments can be in a line of its own,
print(2+2)  # but can also be in teh same line as code

print('contrary to code, comments are not executed')
# -

#    C. **(Optional)** For each new thing that your code in Task 4B does, output a line explaining what just happened (use ```print()```). 


