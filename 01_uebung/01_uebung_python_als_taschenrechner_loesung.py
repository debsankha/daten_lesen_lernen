# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_    
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Lösungen zu Übung 1: Python als Taschenrechner

# 2.A
print(1 + 1)
print(1 - 1)
print(3 * 4)
print(10 / 5)
print(2 ** 8)

# +
# 2.B
ergebnis1 = 10 * 3 + 2
ergebnis2 = 10 * (3 + 2)

print(ergebnis1)
print(ergebnis2)

# +
# 2.C (Optional)
ergebnis1 = 10/3
print('Gleitkommazahl')
print(ergebnis1)
print(type(ergebnis1))

print() # "print()" ohne Text erzeugt einfach eine Leerzeile
print('Ganzzahl')
ergebnis2 = int(10 / 3)
print(ergebnis2)
print(type(ergebnis2))
# -

# 2.D (Optional)
print(0.1 + 0.2) # ergibt nicht exakt 0.3!

# +
# 2.D Fortsetzung

# das können wir bestätigten, indem wir die
# Zahlen auf Gleichheit testen (das kommt
# später noch genauer dran). Die Antwort
# "False" bedeutet, dass für den Computer
# diese beiden Zahlen NICHT gleich sind
0.1 + 0.2 == 0.3
# -

# 3.A
vorname = 'Emmy'
nachname = 'Noether'
zitat = '''Das erste und einzige Mal, dass 
man Emmy Noether in Deutschland ohne jede 
Einschränkung ihren männlichen (beamteten) 
Kollegen gleichstellte, war, als die 
Nationalsozialisten sie aufgrund des Gesetzes 
zur Wiederherstellung des Berufsbeamtentums ... 
am 25. April 1933 von ihrer Tätigkeit an der 
Universität Göttingen beurlaubten, obwohl sie 
als nichtbeamtete Professorin zu diesem 
Zeitpunkt strenggenommen vom Gesetz noch 
gar nicht betroffen war.'''

# 3.B
satz = vorname + nachname + zitat
print(satz)

# 3.C
print('Über {} {}: "{}"'.format(vorname, nachname, zitat))

# +
# 4.A und 4.B

# definiere eine Reihe von Variablen und
# speichere ganze Zahlen in ihnen
a = 2
b = 10

# berechne das Produkt von a und b
produkt = a * b

# gib das Ergebnis in einem formatierten String aus
print('Das Produkt von {} und {} ist {}.'.format(a, b, produkt))

# berechne den Quotient von a und b
quotient = a / b

# gib das Ergebnis in einem formatierten Strig aus
print('Die Rechnung {} / {} ergibt {}.'.format(a, b, quotient))

# gib auch den Typ (Gleitkommazahl oder "float")
# des Ergebnisses aus
print('Das Ergebnis ist dabei ein {}'.format(type(quotient)))
