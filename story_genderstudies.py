# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline

# ```Das Tutorium für die Studierenden der Gender Studies und Sozialwissenschaften findet Donnerstags von 16 bis 18 Uhr im VG 1.106 statt und wird von Jana Lasser und mit Unterstützung von Daniela Heitzmann durchgeführt.
# Im Tutorium werden wir uns verstärkt mit von der UN zur Verfügung gestellten Datensätzen, mit denen z.B. der Fortschritt der sustainable development goals, insbesondere Geschlechtergerechtigkeit und reduzierte Ungleichheit beschäftigen, aber auch Daten zur Migration werden Thema sein. Anhand der Arbeit mit diesen Datensätzen sollen grundlegende Methoden und Konzepte im Umgang mit Daten illustriert und geübt werden. Dabei geht es um die automatisierte Bearbeitung, die Visualisierung und das Verstehen von statistischen Zusammenhängen von Daten. Wer schon immer einmal nachvollziehen wollte, wie sich ein Index, der Geschlechtergerechtigkeit misst, eigentlich zusammensetzt und auf welchen Daten er basiert, oder wer sich bei der Einschätzung von Herausforderungen in der Migration nicht auf widersprüchliche Berichte aus der Politik oder den Medien verlassen möchte, der ist in diesem Tutorium richtig!```

data = pd.read_csv('daten/genderstudies/Indicator_5.5.2__Proportion_of_women_in_managerial_positions_percent.csv')
data.head()

# ## What is in the data?

# find germany
data.index[data_female['geoAreaName'] == 'Germany'].tolist()

# which entries contain the time resolved data?
data_ger = data.loc[83][13:30]
data_ger

years = range(2000,2017)
plt.plot(years,data_ger, label='Germany')
plt.xlabel('year')
plt.ylabel('percentage')
plt.title('women in managerial positions')
plt.legend()

# Exercise: annotate the plot with axis labels, a title and a legend. HINT: ```plt.xlabel()``` creates a label for the x-axis (see documentation [here](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.xlabel.html). The label for the y-axis and the title works similar. 

# Exercise: this plot now looks like there are huge changes going on in Germany. But is that really so? How much is the total change of women in managerial positions in Germany between the years 2000 and 2016? How could you display the graph to make the real change clearer? HINT: ```plt.ylim(lower, upper)``` sets the extent of the y-axis which is shown to the range [lower, upper].

# +
# solution
plt.plot(years,data_ger, 'o-',label='Germany')
plt.ylim(0,55)
plt.xlim(2000, 2016)
plt.xlabel('year')
plt.ylabel('percentage')
plt.title('women in managerial positions')

plt.plot([1999, 2017], [50, 50], '--', color='black')
plt.text(2007, 47, 'parity')

plt.legend()


# -

# find dountry
def get_country_data(name, data):
    print(country)
    idx = data.index[data_female['geoAreaName'] == name].tolist()[0]
    country_data = data.loc[idx][13:30]
    return country_data


# * Exercise: use the above function to plot the time development for three different countries. HINT: try countries where continuous data exists, for example Germany, Australia and Uruguay.  
# * Exercise: what does it mean if the line in the plot is not continuous? How do the corresponding entries in the table look like?  
# * Exercise optional: can you explain the huge drup between the year 2010 and 2011 in Uruguay?  
# * Question: gender gap index? (Wikipedia: global gender gap report)
# * Idea: see how rounding affects positions in table
# * Idea: re-plot histograms for sub-indices of gender gap report

# +
# interesting countries 
# Azerbaijan, Argentina, Australia, Austria, Brazil, Thailand, 
# Turkey, Ukraine, Egypt, Uruguay, 

for country in ['Germany', 'Australia', 'Uruguay']:
    plt.plot(years, get_country_data(country, data), 'o-', label=country)


plt.xlabel('year')
plt.ylabel('percentage')
plt.title('women in managerial positions')
plt.legend();
# -

# ## Data quality

# +
# number of countries
print(len(data))

# problem: see http://www.worldometers.info/geography/how-many-countries-are-there-in-the-world/
# -> rather continents and territories
# -

# How many countries have data at all?

# which of the countries have NaN as entry in 'last_entry'?
nodata = pd.isnull(data['latest_year']).sum()
print('{} out of {} have not a single entry'.format(nodata, len(data)))

data.head()

# Go a bit deeper: can we find out, which continents have the best/worst data coverage?

# to find out about that, we need a mapping of country to continent.
# for this, we load a file that provides us with exactly that
# data from https://datahub.io/JohnSnowLabs/country-and-continent-codes-list
country_codes = pd.read_csv('daten/genderstudies/country-and-continent-codes-list.csv')
country_codes.head(50)


# We use the ```ISO3CD``` entry in the table that contains a three-letter country code for each country. This three-letter code can be matched to the list of countries that also contains their respective continent affiliation:

# +
continents = []
for index, row in data_female.iterrows():
    code = row['ISO3CD']
    cont_idx = country_codes.index[country_codes['Three_Letter_Country_Code'] == code].tolist()
    cont_idx = cont_idx[0]
    continent = country_codes['Continent_Name'][cont_idx]
    #print(continent)
    
    continents.append(continent)

# add the continent affiliation to the original table as a new column
data['Continent'] = continents
data.head()

# +
# now we want to find out, how many asian countries have data
mask_asia = data_female['Continent'] == 'Asia'
data_asia = data_female[mask_asia]

mask_asia_nodata = pd.isnull(data_asia['latest_year'])
data_asia_nodata = data_asia[mask_asia_nodata]

print('there is data from {} out of {} asian countries'\
     .format(len(data_asia) - len(data_asia_nodata), len(data_asia)))
# -

# Exercise: make the above routine more general and check data availability for all seven continents using a ```for``` loop. 

continents = set(data_female['Continent'])
for continent in continents:
    mask_continent = data_female['Continent'] == continent
    data_continent = data_female[mask_continent]
    
    mask_continent_nodata = pd.isnull(data_continent['latest_year'])
    data_continent_nodata = data_continent[mask_continent_nodata]
    
    print('there is data from {} out of {} countries in {}'\
     .format(len(data_continent) - len(data_continent_nodata),\
             len(data_continent), continent))
    print()

continents = list(set(data_female['Continent']))
percentages = []
for continent in continents:
    mask_continent = data_female['Continent'] == continent
    data_continent = data_female[mask_continent]
    
    mask_continent_nodata = pd.isnull(data_continent['latest_year'])
    data_continent_nodata = data_continent[mask_continent_nodata]
    
    perc = (len(data_continent) - len(data_continent_nodata)) / \
        len(data_continent) * 100
    
    percentages.append(perc)

# +
data_coverage = pd.DataFrame()
data_coverage['continent'] = continents
data_coverage['percentage'] = percentages

data_coverage
# -

data_coverage = data_coverage.drop([1])

plt.bar(data_coverage['continent'],data_coverage['percentage'])

# ## Questions
# * Female participation and religion: correlate state religion to percentage of females in managerial positions. -> Auflistung der Religionsmehrheitsverhältnisse pro Land für Korrelation?
# * Female participation and recent wars: correlate the occurence of a war in the recent history of a country to female participation. 
# * Correlation of females in managerial position and females in parliaments?
# * What are the absolute values?
# * Noise vs. relevance of trends
# * reverse Data, display male percentages or show both in the same plot
# * plot data over world map

# ## Correlation with percentage of women in parliaments

par_data = pd.read_csv('daten/genderstudies/API_SG.GEN.PARL.ZS_DS2_en_csv_v2_10515251.csv')
par_data = par_data.rename(index=str, columns={'Country Code': 'ISO3CD'})
par_data.head()

# +
man_data = data
merged_data = pd.merge(man_data, par_data, how='inner',on='ISO3CD')
#merged_data.dropna(inplace=True)


par_man = pd.DataFrame()
par_man['parliament'] = merged_data['2018']
par_man['management'] = merged_data['latest_value']
par_man.dropna(inplace=True)
len(par_man)
# -

merged_data.head()

plt.scatter(par_man['management'], par_man['parliament'])
plt.xlabel('women in managerial positions / %')
plt.ylabel('women in parliament / %')

# +
from scipy.stats import linregress
import numpy as np

slope, intercept, rvalue, pvalue, stderr = \
    linregress(par_man['parliament'][mask], par_man['management'][mask])

print(stderr)
# -

xvalues = np.arange(0,50,0.1)
plt.scatter(merged_data['F2016'], merged_data['2018'])
plt.plot(xvalues, intercept + slope * xvalues)
plt.xlabel('women in managerial positions / %')
plt.ylabel('women in parliament / %')

# ## Correlation with women in senior managerial positions

sen_man_data = pd.read_csv('daten/genderstudies/Indicator_5.5.2__Proportion_of_women_in_senior_and_middle_management_positions_percent.csv')
sen_man_data.head()

plt.scatter(man_data['latest_value'], sen_man_data['latest_value'])

man_vs_senman = pd.DataFrame()
man_vs_senman['managerial'] = man_data['latest_value']
man_vs_senman['senior managerial'] = sen_man_data['latest_value']
man_vs_senman.dropna(inplace=True)
man_vs_senman.head()

slope, intercept, rvalue, pvalue, stderr = \
    linregress(man_vs_senman['managerial'], man_vs_senman['senior managerial'])
print(pvalue)

xvalues = np.arange(0,50,0.1)
plt.scatter(man_data['latest_value'], sen_man_data['latest_value'], label='data')
plt.plot(xvalues, intercept + slope * xvalues, label='fit')
plt.xlabel('women in managerial positions / %')
plt.ylabel('women in middle and senior managerial positions')
plt.legend()
#plt.text(5, 40, 'p = {:1.7f}'.format(pvalue))

# ## Scrape data on the existence of Quotas from IDEA database

# data from https://www.idea.int/data-tools/data/gender-quotas/country-overview
import requests

url = 'https://www.idea.int/data-tools/data/gender-quotas/country-overview'
text = requests.get(url).text

from bs4 import BeautifulSoup
soup = BeautifulSoup(text,'lxml')
print(soup.prettify())

my_table = soup.find('table',{'class':'search-results gq-search-results'})

my_table

# +
countries = []
for a in my_table.find_all('a', href=True):
    countries.append(a.contents[0])
    
countries
# -

for a in my_table.find_all('td'):
    print(a)

# +
#new_table = pd.DataFrame(columns=range(0,4), index = [0]) # I know the size 
results = []
percentage_women = []


row_marker = 0
for row in my_table.find_all('tr')[1:]:
    #print(row_marker)
    column_marker = 0
    columns = row.find_all('td')
    
    #print(row)
    results.append(columns[2])
    percentage_women.append(columns[3])
    #for column in columns:
        #print(column_marker)
        #print(column)
    #    new_table.iat[row_marker,column_marker] = column.get_text()
    #    column_marker += 1
    row_marker += 1

#new_table
# -

perc = [float(e.get_text().split('%')[0]) for e in percentage_women]

women = [int(e.get_text().split(' ')[0]) for e in results]
sizeof_parl = [int(e.get_text().split(' ')[2]) for e in results]

# +
quota_data = pd.DataFrame()
quota_data['country'] = countries
quota_data['size of parliament'] = sizeof_parl
quota_data['women in parliament'] = women
quota_data['percentage of women in parliament'] = perc

quota_data.head()

quota_data.to_csv('daten/genderstudies/quota_data.csv')
# -

# ## Aggregate managerial data with quota existence
# Note: aggregation so far failed. Would need to associate country names with three-letter country short codes. This was not successful so far, because some country names are not consistent. This could be an exercise.

country_codes.head()

short_names = [e.split(',')[0] for e in country_codes['Country_Name']]
country_codes['Country_Short_Name'] = short_names
country_codes.head(60)

quota_data['country'][23] = 'Congo, Democratic Republic of the'

# +
codes = []

for country in quota_data['country']:
    #print(country)
    try:
        idx = country_codes.index[country_codes['Country_Short_Name'] == country].tolist()[0]
    except IndexError:
        print(country)
# -

# ## Birth rathes

birth_data = pd.read_csv('daten/genderstudies/Indicator_3.7.2__Adolescent_birth_rate_per_1000_women_aged_1519_years.csv')
birth_data.head()

birth_data.loc[10]#[15:34]

years = range(2000,2019)
plt.plot(years, birth_data.loc[1][15:34], 'o-')

# ### Questions / ideas
# * time-development of adolescent birth rates, averaged by continents

# ## Maternal mortality ratio

mort_data = pd.read_csv('daten/genderstudies/Indicator_3.1.1__Maternal_mortality_ratio.csv')
mort_data.head()

mort_data['geoAreaName']

index = mort_data.index[mort_data['geoAreaName'] == 'United States of America'].tolist()[0]
print(index)
mort_data.loc[index][13:44]

years = range(1985, 2016)
#plt.plot(years, mort_data.loc[401][13:44], 'o-', label='Ethiopia')
plt.plot(years, mort_data.loc[455][13:44], 'o-', label='Germany')
plt.plot(years, mort_data.loc[3][13:44], 'o-', label='Albania')
plt.plot(years, mort_data.loc[338][13:44], 'o-', label='China')
plt.plot(years, mort_data.loc[179][13:44], 'o-', label='US')
plt.xlabel('years')
plt.ylabel('mortality per 100.000 live births')
plt.legend()

# ### Questions
# * Which countries have the highest drop?
# * Are there countries with a rise? Why could that be?

# ## Migration questions
# * bezogen auf Deutschland
# * human development index


