# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Exercise 4
# ### Some tips
# $\rightarrow$ You should take a look at the exercises at home and try to solve as many tasks as you can. In the tutorials you could go on with solving the tasks you could not finish at home and if necessary, ask for help from the tutors. 
#
# $\rightarrow$ You you encounter an error:
# 1. _Read_ and _understand_ the error message. 
# 2. Try to find out the solution to the problem ($\rightarrow$ this may be hard at the beginning, but it is very educational!)
# 3. Search using a search engine for the problem (hint: Stackoverflow) or ask your neighbour!
# 4. Ask the tutors.
#
# $\rightarrow$ Look out for the sign <font color='green'>**HINT**</font>: these are hints that helps you with solving the task and sometimes additional information regarding the task. 
#
# $\rightarrow$ Tasks marked **(Optional)** are for those of you who are extra fast :-).

# <a name="top"></a>Contents
# ---
# * [Tasks](#aufgaben)
# * [Supplement: images](#bilder)
# * [Supplement: Text](#text)

# <a name="aufgaben"></a> Functions, images and text
# ---
# For the topics "Images" and "Text", there's no video lectures. That's why we have some supplements with short introductions on these topics, that you  may look at before attempting the tasks on these two topics. 
#
# 1. **Functions**
#   1. Write a function that takes two numbers as arguments and prints their sum, difference, product and quotient on the console (use `print()`). Test the function with different sets of arguments. 
#   2. Write another function, that accepts a list of numbers as its argument, and returns (not prints on console, use `return bla`) the sum of the squares of the numbers in the list. Test the function with different lists of numbers. 
#   3. **(Optional)** Write another function, that takes a list of numbers as argument, sorts them in increasing order (you are not allowed to use the function `sort()`, otherwise it'd be boring :D). Test the function with different lists. What's the problem if you apply this function on a long list?
# 2. **Bilder**  
# <font color='green'>**HINT:** For the following section, you need to download the picture "cat_image.JPG" from the folder on StudIP and upload to JupyterHub (preferably in a separate folder named `data` or something similar). </font>
#   1. Load the image "cat_image.JPG" using the library ```PIL```, transform it into an array using ``NumPy``` and display it using ```matplotlib.pyplot``` (exactly as the supplement does it).
#   2. Display only the face of the cat, by slicing a section of the array. 
#   3. Store the result in a separate array
#   4. **(Optional)** Create a grayscale image, display it using matplotlib, and save it as am new image. [Here](https://stackoverflow.com/questions/12201577/how-can-i-convert-an-rgb-image-into-grayscale-in-python) you will find various ways of doing this using python. Note: there's so single right answer here. 
#   
# 3. **Text**  
# <font color='green'>**HINT:** For the following section, you need to download the text file "GG.txt" from the folder on StudIP and upload to JupyterHub (preferably in a separate folder named `data` or something similar). The text is the German constitution.</font>
#
#   1. Load the text from "GG.txt" using the function `readlines()` (exactly as described in the supplement).
#   2. Find out how many times do the words "Freiheit" and "Gott" appear. Do the same with other words that interest you. 
#   3. Remove the character ```\n``` from the text, it denotes a line break. Count how many characters are there now.
#   <font color='green'>**HINT**: In order to change the value of an element of a list, you need to access it bhy its index. For doing that, the function `enumerate()` is helpful. [See  the documentation](http://book.pythontips.com/en/latest/enumerate.html). Here's an illustration of what the problem is:</font>

# +
# create test list
test_list = ['a', 'b', 'c', 'd', 'f']
print('original list: {}'.format(test_list))

# the following does not actually change
# the list, only the temporary variable
# `element` gets modified. 
for element in test_list:
    element = 'z'
    
print('first try: {}'.format(test_list))

# this actually changes the list "permanently"
for index in range(len(test_list)):
    test_list[index] = 'z'
    
print('second try: {}'.format(test_list))
# -

# 3. **Text (continued)**  
#   4. How many whitespace chanracters are there in the constitution? What's the average length of words?
#   5. **(Optional)**: Create a table (pandas dataFrame) with two columns: first column being each line of the text, teh second line being the number of characters in that line. Create an additional column, containing the average length of words in that line. Save the table as a .csv file. What problems did you face, in trying to compute the average word lengths? How do the whitespace chanracters behave?
#   6. **(Optional)**: Compare the average word lengths with another text that you fancy. Is it larger or smaller? How can you explain it?

# [Top](#top)

# <a name="bilder"></a> Supplement Images
# ---
# In order to open images in python, we will use the module ```PIL``` (**P**ython **I**mage **L**ibrary, [Documentation](https://pillow.readthedocs.io/en/3.1.x/reference/Image.html)). In order to display the images in the notebook we will use the function ```imshow()``` of the module ```matplotlib.pyplot``` ([Documentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imshow.html)):

# +
# import "Image" from the library PIL
from PIL import Image

# import the submodule pyplot from the 
# module matplotlib under the alias "plt"
import matplotlib.pyplot as plt

# The following command is necessary in
# order to display images directly in the
# notebook
# %matplotlib inline

# load the image with teh function Image.open()
# and store under the variable `image`. 
# HINT: the following command only works if you
# store the image in the folder "data". If you store
# the image in the same directory as this notebook, 
# use the alternative
# image = Image.open('cat_image.JPG')
image = Image.open('data/cat_image.JPG')

# show the image
plt.imshow(image)
# -

# We can now easily get an overview of various properties of the image.
# Digital images consists of individual points (pixels), each of which
# contains three values for three base colours: red, green and blue. 
# These pixel values are arranged in a 2-dimensions, in a so-called
# "Matrix". In python, we call them "arrays". 
#
# In order to convert the image we loaded into an array, we will use 
# the function ```asarray()``` from the library ```NumPy``` ([Documentation](https://docs.scipy.org/doc/numpy/reference/generated/numpy.asarray.html)):

# +
# import die library numpy under the 
# alias "np"
import numpy as np

# convert the image into an array
image = np.asarray(image)
# -

# Numpy arrays have *a lot* of functionalities built in (see [here](https://docs.scipy.org/doc/numpy/reference/generated/numpy.array.html)), using which we can extract many pieces of information about the image:

# `shape` tells us how many pixels the image has
# in each dimension (width and height). This image
# is 3632 pixels tall and 5456 pixels wide and
# 3-element "deep". The "depth" of 3 means we have
# a colour image, and each pixel contains values
# for there base colours (red, green and blue)
print(image.shape)

# +
# Colour values in digital images can contain values
# between 0 and 254. We can see this by checking what
# are the maximum and minimum values in the array
# `image`

print('maximum colour value: {}'.format(image.max()))
print('minimim colour value: {}'.format(image.min()))
print('average colour value: {}'.format(image.mean()))
# -

# Selecting a part of the image is very easy, by "slicing"
# the array

# Show only the top half of the image
plt.imshow(image[0:1500,0:,0:])

# show only one colour channel (red)
plt.imshow(image[0:,0:,0])

# Finally, we can save the image as a new image file using the function ```imsave()```([Documentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imsave.html)).

# +
# display the image
plt.imshow(image[0:,0:,0])

# save the image with the name cat.png
plt.savefig('data/cat.png')
# -

# [Top](#top)

# <a name="text"></a> Supplement: Text 
# ---
# In order to load text files, we will use the built-in python function
# ```open()``` ([Documentation](https://docs.python.org/3/library/functions.html#open)). This returns a so-called "file handle". In order to read the contents of the file, we need to apply the function ```readlines()``` ([Documentation](https://docs.python.org/2.4/lib/bltin-file-objects.html)) on the file handle, which "reads" the file and returns a list of strings, each element being each line in the file. 

text_file = open('data/GG.txt')
text = text_file.readlines()

print(text)
# Hint: the character "\n" denotes a line break 
# and is there many times in this text

# The number of lines is very easy to find out:

len(text)

# So is the length of each line

line_lengths = [len(line) for line in text]
print(line_lengths)

# And the total number of characters in the constitution:

print('the german constitution has {} characters.'\
      .format(sum(line_lengths)))

# Let's find out the number of articles in the constitution. We do that using the function ```count()``` 

articles = [line.count('Art') for line in text]
sum(articles)

# In order to save the constution in a form more useful for further analysis, 
# let's save it as a table (Pandas ```DataFrame```) and export with the function ```to_csv()``` ([Documentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html)) to a .csv file.

# +
# import the module "Pandas" under the alias pd
import pandas as pd

# create a table with the lines of teh constitution
# as the first columns and line lengths as the second column
table = pd.DataFrame({'line':text, 'length': line_lengths})
# -

# show the first few lines of the table
table.head()

# access only the 'length' column
table['length']

# save the DataFrame
table.to_csv('data/grundgesetz.csv')


