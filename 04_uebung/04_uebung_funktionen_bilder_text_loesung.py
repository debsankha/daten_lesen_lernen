# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Lösungen zu Übung 4: Funktionen, Bilder und Text

# <a name="top"></a>Inhalt
# ---
# * [Funktionen](#funktionen)
# * [Bilder](#bilder)
# * [Text](#text)

# <a name="funktionen"></a> Funktionen
# ---

# 1.A
# Erstelle die Funktion mit dem Namen 
# "meine rechenfunktion". Die Funktion
# bekommt zwei Argumente, "zahl1" und "zahl2".
# Sie berechnet in ihrem Inneren Summe,
# Differenz, Produkt und Quotient der beiden
# Argumente und gibt sie jeweils aus.
def meine_rechenfunktion(zahl1, zahl2):
    summe = zahl1 + zahl2
    print('{} + {} = {}'.format(zahl1, zahl2, summe))
    
    differenz = zahl1 - zahl2
    print('{} - {} = {}'.format(zahl1, zahl2, differenz))
    
    produkt = zahl1 * zahl2
    print('{} * {} = {}'.format(zahl1, zahl2, produkt))
    
    quotient = zahl1 / zahl2
    print('{} / {} = {}'.format(zahl1, zahl2, quotient))


# 1.A Fortsetzung
# WICHTIG: die bloße Definition der Funktion
# in der obenstehenden Code-Zelle hat noch zu
# keiner Ausgabe geführt. Aber unser Script "kennt"
# die Funktion "meine_rechenfunktion" jetzt, und wir
# können sie nachfolgend benutzen. Wir testen also
# die Funktion mit verschiedenen Argumenten:
meine_rechenfunktion(6, 10)
print()
meine_rechenfunktion(100956, 79348)
print()
meine_rechenfunktion(10,10)


# 1.B
# Definiere eine neue Funktion mit dem Namen
# "quadratsumme". Diese Funktion bekommt nur
# ein Argument und zwar eine Liste. Sie berechnet
# das Quadrat jedes Elementes in der Liste und
# summiert die Zwischenergebnisse auf (sie werden
# in der nur innerhalb der Funktion bekannten 
# Variablen "summe" gespeichert).
# Statt das Rechenergebnis direkt
# auszugeben, gibt sie es diesmal "zurück" (siehe
# das "return"-Keyword). Wir können also das Ergebnis
# der Funktion einer neuen Variable zuweisen
def quadratsumme(liste):
    summe = 0
    
    for zahl in liste:
        summe = summe + zahl * zahl
        
    return summe


# 1.B Fortsetzung
# teste die Funktion mit einer kurzen Liste
ergebnis = quadratsumme([1,2,3,4,5])
print(ergebnis)


# 1.C 
# Implementierung des Sortier-Algorithmus
# "Bubblesort" 
# (siehe https://de.wikipedia.org/wiki/Bubblesort)
def bubbleSort(liste):
    
    # erzeuge eine Liste der Durchlaeufe durch
    # die Liste. Bei einer Liste mit 5 Elementen
    # ist z.B. durchlaeufe = [5,4,3,2,1]
    durchlaeufe = range(len(liste) - 1, 0, -1)
    
    # iteriere über alle "durchlaeufe"
    for durchlauf in durchlaeufe:
        
        # iteriere über jeden Durchlauf
        # im 3. Durchlauf wird hier z.B: über
        # die Liste [0,1,2] iteriert
        for i in range(durchlauf):
            
            # wenn das Element auf Listenindex
            # i größer als das Element auf Index
            # i + 1 ist, vertausche die Beiden
            # Elemente. Vertauschen wird realisiert,
            # indem das Element auf Index i in der 
            # Variable "temp" zwischengespeichert wird
            if liste[i] > liste[i + 1]:
                temp = liste[i]
                liste[i] = liste[i + 1]
                liste[i + 1] = temp


# 1.C Fortsetzung
# lange Liste: dauert sehr lange. 
# Genauer: dieser Sortieralgorithmus
# skaliert mit n^2, wobei n die Anzahl der
# Elemente in der Liste ist. Besser wäre
# z.B. "Quicksort" oder "Mergesort", die
# mit n * log(n) skalieren.
bubbleSort(range(10000,0,-1))

# [Anfang](#top)

# <a name="funktionen"></a> Bilder
# ---

# +
# 2.A
# importiere die benötigten Bibliotheken
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

# lade das Bild mit der Funktion open()
# und speichere es in der Variable "bild"
bild = Image.open('daten/cat_image.JPG')

# konvertiere das Bild in ein NumPy array
bild = np.asarray(bild)

# zeige das Bild an
plt.imshow(bild)
# -

# 2.B ausschneiden
# Das Katzengesicht beginnt in vertikaler Richtung
# bei etwa 1000 Pixeln und hört be 2050 Pixeln auf:
# 1000:2050. In horizontaler Richtung beginnt es bei 
# 1500 Pixeln und hört bei 2550 auf: 1500:2550.
# In der dritten Dimension (den Farbwerten), nehmen 
# wir alle drei Werte (rot, grün und blau) mit, 
# indem wir das offene Intervall 0: angeben.
gesicht = bild[1000:2050,1500:2550,0:]
plt.imshow(gesicht)

# +
# 2.C
from os.path import join
ordner = "daten"
bildname = "katzengesicht.png"

# zum Speichern im Unterordner "daten" geben wir 
# den Pfad dorthin an. Den korrekten Pfad erzeugen
# wir betriebssystem-unabhängig indem wir die 
# Funktion "join()" aus dem Modul "os.path"
# benutzen. Alternativ können wir auch "daten/katzengesicht.png"
# unter Linus oder "daten\katzengesicht.png" unter
# Windows als Pfad angeben.
plt.savefig(join(ordner, bildname))

# +
# 2.D (optional) Graustufen

# erzeuge das Graustufenbild indem wir alle drei
# Farbwerte je Pixel mitteln (aufsummiereun und
# dann durch 3 teilen). Das machen wir mit der 
# Funktion "sum()" des arrays in dem das Bild
# gespeichert ist. Um klar zu machen, über welche
# Dimension (Achse) summiert werden soll, geben wir 
# der Funktion die Achse explizit als sog. 
# "keyword argument" mit. Die nullte Achse wäre hier
# die vertikale Richtung, die erste Achse die 
# horizontale Richtung und die zweite Achse steht
# für die Farbwerte. Mit "axis=2" wird also über
# die zweite Achse summiert
grayscale = bild.sum(axis = 2) / 3.

# um das Bild auch in Graustuften anzuzeigen, müssen
# wir der Funktion "imshow()" noch explizit mitteilen,
# welche Farbtabelle (colormap) sie verwenden soll.
# Tun wir das nicht, verwendet sie die default-colormap
# die das Bild in Falschfarben anzeigt. Mit dem
# keyword-argument cmap='gray' teilen wir also mit, dass
# eine Farbtabelle mit graustufen verwendet werden soll.
plt.imshow(grayscale, cmap = 'gray')

# wie im Aufgabenteil oben speichern wir das Bild im
# ordner "daten", indem wir den ganzen Pfad dorthin
# angeben.
bildname = 'katze_graustufen.png'
plt.savefig(join(ordner, bildname))
# -

# [Anfang](#top)

# <a name="funktionen"></a> Text
# ---

# 3.A
# als erstes laden wir das Grundgesetz wieder als Text
text_file = open('daten/GG.txt')
text = text_file.readlines()

# +
# 3.B Worte zählen

# speichere das Wort, nach dem gesucht werden soll in
# der Variable "wort"
wort = 'Freiheit'

# zähle die Vorkommnisse des Wortes für jede Zeile,
# indem über alle Zeilen des Textes iteriert wird.
# Die Vorkommnisse werden in einer eigenen Liste
# mit dem Namen anzahl_wort gespeichert.
anzahl_wort = [zeile.count(wort) for zeile in text]

# gib das Ergebnis aus
print('Das Wort {} kommt {} mal im Grundgesetz vor.' \
     .format(wort, sum(anzahl_wort)))

# +
# 3.C entferne alle Zeilenumbrüche

# iteriere über alle Zeilen. Die Funktion "enumerate()"
# liefert sowohl den Index der Zeile ("i") als auch die 
# Zeile selbst ("zeile"). Um Werte in der Liste der Zeilen
# zu verändern, müssen wir auf sie über ihren Index zugreifen:
for i,zeile in enumerate(text):
    text[i] = zeile.replace('\n','')
    
# wir zählen wieder die Anzahl der Zeichen je Zeile
zeichen = [len(zeile) for zeile in text]

# und rechnen die Gesamtanzahl aus 
# (vorher: 184855 Zeichen)
anzahl_zeichen = sum(zeichen)
print('Das Grundgesetz ohne Zeilenumbrüche hat {} Zeichen.'\
      .format(anzahl_zeichen))

# +
# 3.D durchschnittliche Wortlänge
# erstelle eine Liste mit der Anzahl der Leerzeichen
# je Zeile
leerzeichen = [zeile.count(' ') for zeile in text]

# summiere über die Liste, um die Gesamtanzahl der 
# Leerzeichen im Text zu errechnen
anzahl_leerzeichen = sum(leerzeichen)
print('Das Grundgesetz hat {} Leerzeichen.'\
      .format(anzahl_leerzeichen))

# erreichne die durchschnittliche Wortlänge als die Anzahl
# aller Zeichen minus die Anzahl der Leerzeichen, dividiert
# durch die Anzahl der Leerzeichen. Dabei machen wir uns das
# Wissen zunutze, dass zwischen zwei Worten immer ein 
# Leerzeichen steht.
wortlaenge = (anzahl_zeichen - anzahl_leerzeichen) / anzahl_leerzeichen
print('Die durchschnittliche Wortlänge ist {:1.2f} Zeichen.'\
      .format(wortlaenge))

# Hinweis: das Kürze ":1.2f" beschränkt die Ausgabe einer
# Zahl auf zwe Dezimalstellen nach dem Komma.

# +
# 3.E (optional) Wortlänge je Zeile

# importiere pandas
import pandas as pd

# errechne die Anzahl der zeichen je Zeile
zeichen = [len(zeile) for zeile in text]

# erstelle eine Tabelle (DataFrame) mit je einer Spalte 
# für die Textzeilen und die Anzahl der Zeichen je Textzeile
tabelle = pd.DataFrame({'zeilen':text, 'zeichen':zeichen})

# erstelle eine leere Liste, um die Wortlänge zu speichern
wortlaenge = []

# iteriere über alle Zeilen
for zeile in text:
    
    # zähle die Anzahl der Leerzeichen je Zeile
    # addiere 1, um später Division durch 0 zu
    # vermeiden
    leerzeichen = 1 + zeile.count(' ')
    
    # zähle die Anzahl der Zeichen je Zeile
    # addiere 1, um später negaitve Ergebnisse
    # zu vermeiden.
    zeichen = 1 + len(zeile)
    
    # errechne die Wortlänge je Zeile wie oben
    laenge = (zeichen - leerzeichen) / leerzeichen
    
    # füge das Ergebnis für die aktuelle Zeile
    # der Liste hinzu
    wortlaenge.append(laenge)

# füge die errechnete wortlänge je Zeile zu der
# Tabelle hinzu
tabelle['wortlaenge'] = wortlaenge

# zeige die ersten paar Zeilen der Tabelle an
tabelle.head()

ordner = 'daten'
tabellenname = 'grundgesetz_wortlaenge.csv'
tabelle.to_csv(join(ordner, tabellenname))

# +
# 3.D (optional) Vergleich der Wortlänge mit einem andere Text

# lade den Huckleberry-Finn Text wie in der Vorlesung 
# beschrieben aus dem Internet herunter. Dafür werden
# die Bibliothek "urllib" für das Herunterladen sowie
# die Bibliothek "re" für Zeichenersetzung im Text
# verwendet. Wie schon im Grundgesetz, entfernen wir
# alle Zeilenumbrüche und carriage-returns aus dem Text
from urllib.request import urlopen 
import re

# Hier definieren wir uns eine kleine Funktion, die 
# das Herunterladen und die Zeichenersetzung erledigt
# und den fertigen Text zurückliefert.
def read_url(url): 
    return re.sub('\\s+', ' ', urlopen(url).read().decode())

# lies den Text von der vorgegebenen URL
# HINWEIS: dieses Kommando funktioniert nur, 
# wenn dein Rechner eine Internetverbindung hat. Da
# du aber wahrscheinlich ohnehin auf dem Jupyter-Hub
# arbeitest, sollte das gegeben sein
huck_finn_url = 'https://www.inferentialthinking.com/data/huck_finn.txt'
huck_finn_text = read_url(huck_finn_url)

# zähle die Zeichen je Zeile und summiere über die Zeilen
zeichen = [len(zeile) for zeile in huck_finn_text]
zeichen = sum(zeichen)

# zähle die Leerzeichen je Zeile und summiere über die Zeilen
leerzeichen = [zeile.count(' ') for zeile in huck_finn_text]
leerzeichen = sum(leerzeichen)

# errechne die Wortlänge
wortlaenge = (zeichen - leerzeichen) / leerzeichen
print('Die durchschnittliche Wortlänge ist {:1.2f} Zeichen'\
     .format(wortlaenge))
# -

# Wie Wortlänge ist mit 4.15 deutlich kleiner als im deutschen Grundgesetz (7.13). Das kann zum einen daran liegen, dass die Sprache eine andere ist (Englisch vs. Deutsch). Zum anderen kann es auch daran liegen, dass es sich bei dem Grundgesetz um einen juristischen Text handelt, der vielleicht etwas kompliziertere und längere Wörter beinhaltet als eine Kindergeschichte. Ein guter weiterer Vergleich, um diese Hypothese zu überprüfen wäre z.B. ein Vergleich mit der Amerikanischen Verfassung oder einem Deutschen Märchen.
#
# Natürlich könnte der Unterschied auch daran liegen, dass die Texte unterschiedlich strukturiert/formatiert sind: Wenn z.B. ein Text an irgendeiner Stelle große Mengen an Leerzeichen enthält, würde das unser Ergebnis hier stark verfälschen, ohne dass wir es bemerken würden. Ein Test, um dieses Problem auszuschließen wäre, nach Zeichenkombinationen wie ```"   "``` (drei Leerzeichen hintereinander) zu suchen und diese zu entfernen.

# [Anfang](#top)
