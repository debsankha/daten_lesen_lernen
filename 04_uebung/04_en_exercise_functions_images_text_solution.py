# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Solutions to Exercise 4: Functions, images and text

# <a name="top"></a>Contents
# ---
# * [Functions](#funktionen)
# * [Images](#bilder)
# * [Text](#text)

# <a name="funktionen"></a> Functions
# ---

# 1.A
# Create a function named "my_calculator"
# It takes two arguments: "num1" and "num2"
# It prints the sum, differnce, product and
# the quotient of num1 and num2 
def my_calculator(num1, num2):
    summ = num1 + num2
    print('{} + {} = {}'.format(num1, num2, summ))
    
    difference = num1 - num2
    print('{} - {} = {}'.format(num1, num2, difference))
    
    product = num1 * num2
    print('{} * {} = {}'.format(num1, num2, product))
    
    quotient = num1 / num2
    print('{} / {} = {}'.format(num1, num2, quotient))


# +
# 1.A continuation
# the definition of "my_calculator" doesn't
# do anything yet, but our notebook now
# 'knows' the function "my_calculator". Now
# we test our function with various arguments

my_calculator(6, 10)
print()
my_calculator(100956, 79348)
print()
my_calculator(10,10)


# -

# 1.B
# Define a new function with the name "square_sum"
# These function accepts only one argument: a list
# of numbers. The function squares each number and
# sums them up. Then it "returns" the sum of squares
# using the `return ` keyword, intead of printing
# it on the console. We can then store the "return
# value" in a new variable and use it in further
# calculations
def square_sum(li):
    total = 0
    
    for num in li:
        total = total + num * num
        
    return total


# 1.B Continuation
# test the function with a short list
result = square_sum([1,2,3,4,5])
print(result)


# 1.C 
# Implementing the sorting-algorithm
# "Bubble sort" 
# (see https://en.wikipedia.org/wiki/Bubble_sort)
def bubble_sort(li):
    
    # Create a list called "passes" that runs
    # through like this. If li has length 5,
    # then passes = [5,4,3,2,1]
    passes = range(len(li) - 1, 0, -1)
    
    # iterate over passes
    for pa in passes:
        
        # iterate over each pass
        # e.g. in 3rd pass we will
        # iterate over the list [1,2,3]
        for i in range(pa):
            
            # if the i-th element is larger than
            # the i+1-th element, switch them. 
            # switching is done by creating
            # a temporary variable "temp", storing
            # i+1-th element in temp, like so
            if li[i] > li[i + 1]:
                temp = li[i]
                li[i] = li[i + 1]
                li[i + 1] = temp


# 1.C continuation
# let's test our function on a list
# Note: bubble_sort we wrote changes
# the list "in place", instead of
# returning a new list. So to check
# that the sorting worked, we need
# to print the original list once 
# before and once after applying the
# function bubble_sort
l = [9,1,5,3,7,2]
bubble_sort(l)
l

# +
# 1.C Continuation
# Use bubble_sort on a long list.
# It takes a very long time to compute.
# The reason is, this algorithm scales 
# as n^2 (n being teh length of the list). 
# There are other sorting algorithms, like
# quicksort or mergesort, that scale as 
# n * log(n)

long_list = list(range(10000,0,-1))
bubble_sort(long_list)
long_list
# -

# [Anfang](#top)

# <a name="funktionen"></a> Images
# ---

# +
# 2.A
# import the required libraries
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

# load the image cat_image.JPG and store
# in the variable "image"
image = Image.open('daten/cat_image.JPG')

# convert the image into a numpy array
image = np.asarray(image)

# show the image
plt.imshow(image)

# +
# 2.B slicing the image
# The fact of the cat falls in vertical dimention
# between pixels ~1000 and ~2500. The notation 
# 1000: 2500 will be used to "slice" these pixels. 
# In horizontal dimension: between ~1500 and ~2550. 
# In the third dimension, we take all three
# values (i.e all three colour values : red,
# green and blue). We denote taking all values, by
# the open interval "0:", meaning slice upto as many 
# entrys as are there

face = image[1000:2050,1500:2550,0:]
plt.imshow(face)

# +
# 2.C
from os.path import join
folder = "data"
image_name = "cat_face.png"

# we now save the cat face in the folder "data".
# to do this, we need to supply a correct path. 
# in python, we create a path in operation system
# -independent way, we use the function `join`
# from the module `os.path`.
# alternatively, we could say `data/cat_face.png`
# in linux/mac and `data\cat_face.png` in windows. 
plt.savefig(join(folder, image_name))

# +
# 2.D (optional) grayscale

# create the grayscale image by taking the average 
# of all three color channels (red, green and blue)
# taking average means summing them up and dividing
# by 3. We do that by using the function `sum()` on
# the numpy array. In order to tell numpy along which
# dimension (axis), we want to sum, we need to pass 
# the "keyword argument" `axis=2`. If this argument
# is not supplied, then numpy will sum over all
# dimensions and return a single number. We need 
# `axis=2` here, because the 0-th axis is the height
grayscale = image.sum(axis = 2) / 3.

# in order to actually see the image in grayscale, we
# need to ask the function `imshow()` to use an
# appropriate "colormap". If we do not do that,
# `imshow` will display the image in false colours. 
# We need to pass the keyword argument `cmap='gray'`
plt.imshow(grayscale, cmap = 'gray')

# as before, we save the grayscale image in the folder
# `data`, creating the path using `join` function
# ordner "daten", indem wir den ganzen Pfad dorthin
# angeben.
imagename = 'cat_grayscale.png'
plt.savefig(join(folder, imagename))
# -

# [Top](#top)

# <a name="funktionen"></a> Text
# ---

# 3.A
# first, load the constitution from a file
text_file = open('data/GG.txt')
text = text_file.readlines()

# +
# 3.B Count how many times a word appears

# save the variable that you want to search for
# under a variable `word`
word = 'Freiheit'

# Count the frequency of `word` in each line
# For this, we use a "list comprehension" that
# iterates over each line and saves the frequency
# in a new list called freq_word
freq_word = [line.count(word) for line in text]

# print the result
print('The word {} appears {} times in the constitution.' \
     .format(word, sum(freq_word)))

# +
# 3.C removing line breaks

# iterate over all the lines in `text` (remember `text` is
# a list of strings), and remove all line breaks '\n'. 
# We use the function `enumerate()` here on `text`. It 
# iterates over each element of `text` (i.e. each line)
# and gives us a pair (index, line), where line is the
# index'th line in `text`. We then remove "\n"'s from
# `line`, and store the new line as the index-th element
# of text.
for i, line in enumerate(text):
    text[i] = line.replace('\n','')
    
# we again count the number of characters in each line
chars_each_line = [len(line) for line in text]

# and count the total number of characters in all lines 
# (before: 184855 )
num_chars = sum(chars_each_line)
print('The constitution without line breaks has {} characters.'\
      .format(num_chars))

# +
# 3.D average word length
# create a list with number of whitespace characters
# in each line
whitespaces_lines = [line.count(' ') for line in text]

# sum over the list `whitespaces` to get the total
# number of whitepsaces in the whole text
num_whitespaces = sum(whitespaces_lines)
print('The constitution has {} whitespaces.'\
      .format(num_whitespaces))

# now comes computing the average word length. we assume that
# between every two words is always a whitespace, therefore
# the number of whitespaces in the text is equal to the total
# number of words.
# therefore average word length 
# = (total number of characters
#    - total number of whitespaces)/total number of whitespaces

avg_word_length = (num_chars - num_whitespaces) / num_whitespaces
print('The average word length is {:1.2f} characters.'\
      .format(avg_word_length))

# HINT: the specifier "{:1.2}f" rounds off a number to 
# 2 decimal places while printing

# +
# 3.E (optional) word length per line

# import pandas
import pandas as pd

# compute number of characters in each line
chars = [len(line) for line in text]

# create a table (DataFrame) with one column for
# line lines and another column for the number
# of characters
table = pd.DataFrame({'lines': text, 'chars': chars})

# create an empty list to store the number of words
avg_word_lengths = []

# iterate over all the lines
for line in text:
    # count words in each line by counting
    # whitespaces. Add 1 in order to avoid 
    # division by 0 later. 
    num_whitespaces = 1 + line.count(' ')
    
    # compute the total number of characters
    # in each line. Add 1 do avoid negative result 
    # later
    nchars = 1 + len(line)
    
    # errechne die Wortlänge je line wie oben
    avg_word_length = (nchars - num_whitespaces) / num_whitespaces
    
    # append the average word length for current line
    # into teh list word_lengths
    avg_word_lengths.append(avg_word_length)

# create a new column in the table with the 
# average word length information
table['avg_word_length'] = avg_word_lengths


folder = 'data'
csv_name = 'grundgesetz_wortlaenge.csv'
table.to_csv(join(folder, csv_name))


# dispaly first few lines of the table
table.head()

# +
# 3.D (optional) Compare the word lengths with another text

# Download the text Huckleberry Finn as you saw in the
# lectures. We will use the module `urllib` for downloading
# data from a url. We will also use the module `re` for
# some text modification: we will replace all whitespace
# characters with a single space ' '. We did something similar
# with the constitution. But this time, we will make sure that
# if there's two successive whitespace characters e.g. '    ',
# they also get replaced by a single space ' '. 

from urllib.request import urlopen 
import re

# Here we define a small function that downloads the url
# and returns the text as a string
def read_url(url): 
    return re.sub('\\s+', ' ', urlopen(url).read().decode())

# Read the url for Huckleberry Finn. 
# Note: this command woks naturally only
# when your PC has an internet connection. 
# in JupyterHub, that's always true
huck_finn_url = 'https://www.inferentialthinking.com/data/huck_finn.txt'
huck_finn_text = read_url(huck_finn_url)

# Compute the length of each line and sum
num_chars_each_line = [len(line) for line in huck_finn_text]
num_chars = sum(num_chars_each_line)

# count the whitespaces in each line and sum
num_whitespace_each_line \
    = [line.count(' ') for line in huck_finn_text]
num_whitespace = sum(num_whitespace_each_line)

# compute the word lengths
avg_word_length = (num_chars - num_whitespace) / num_whitespace
print('The average word length is {:1.2f} chars'\
     .format(avg_word_length))
# -

# The average word length of Huckleberry Finn (4.15) is quite smaller than the german constitution (6.86). This could be due to the differnce in language. It could also be due to the fact that the constitution employs legal vocabulary with many long words, compared to a childern's novel. A more appropriate comparison would have been between German and the American constitution, or between Huckleberry Finn and a German fairy tale. 
#
# Formatting of the texts may also cause a problem with our calculations.
# We assumed that the number of whitespace characters is equal to the number of words. If for some reason a text file contains many white space characters, it will cuase problems in our computation. In case of Huckleberry Finn, we took care to replace consecutive whitespaces (like '   ') with a single ' ', but we didn't do that with the constitution. 

# [Top](#top)


