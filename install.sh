#!/bin/bash

PRE_COMMIT_CONVERTER_STR=$(cat <<'EOF'
#!/bin/bash
for NOTEBOOK_PATH in $(git diff HEAD --name-only | grep -E "\.ipynb$")
    do
        NOTEBOOK_WO_SUFFIX=$(echo $NOTEBOOK_PATH | rev | cut -c 7- | rev)
        CONVERTED_PYLIGHT_FILE_PATH="${NOTEBOOK_WO_SUFFIX}.py"
        jupytext --from ipynb --to py:light $NOTEBOOK_PATH
        git add $CONVERTED_PYLIGHT_FILE_PATH # necessary: actually add the converted .py files to git
    done
git reset HEAD **/*.ipynb # do not let users ever push ipynb files
EOF
)

POST_MERGE_CONVERTER_STR=$(cat <<'EOF'
#!/bin/bash
# bug: need to exit screaming if jupytext not found

for PYFILE in $(git diff HEAD~ --name-only)
  do
    # check if it's a jupytexted py file
    grep "jupytext_version" $PYFILE \
      && grep "text_representation" $PYFILE \
      && cat $PYFILE | head -n 1 | grep "# ---"
    if [ $? -eq 0 ] # only if it's a jupytext'd .py file
      then
        jupytext --from py:light --to ipynb $PYFILE
    fi
  done
EOF
)

# make sure we are in the correct directory
if [ -d .git ]
  then 
    echo "In a git repository, continuing..."
  else
    echo "Not in a git repository, exiting." && exit 1
fi

GIT_DOC_ROOT=$(git rev-parse --show-toplevel)

echo -e "Going to install pre-commit and post-merge hooks\nfor jupyter notebooks in the repository:\n${GIT_DOC_ROOT}"
read -r -p "Are you sure you want to proceed? [Y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
  then
    echo ""
  else
    echo "Exiting"
    exit 1
fi
# create the hooks
echo "$PRE_COMMIT_CONVERTER_STR" > .git/hooks/pre-commit
echo "$POST_MERGE_CONVERTER_STR" > .git/hooks/post-merge
echo "created the git hooks"

# now we needed to add it an executable, otherwise they won't work
chmod +x .git/hooks/pre-commit
chmod +x .git/hooks/post-merge
# install jupytext
pip install jupytext

# set aliases

echo "Going to set aliases in the virtual environment: ${VIRTUAL_ENV}"
read -r -p "Are you sure you want to proceed? [Y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
  then
    echo ""
  else
    echo "Exiting"
    exit 1
fi

VIRTUALENV_ACTIVATE_FILE="${VIRTUAL_ENV}/bin/activate"

BASH_ALIASES_STR=$(cat <<'EOF'
alias convert_py2ipynb="jupytext --from py:light --to ipynb "
alias convert_ipynb2py="jupytext --to py:light --from ipynb "
EOF
)

# remove existing alises
sed -i '/^alias convert_py2ipynb/d' $VIRTUALENV_ACTIVATE_FILE
sed -i '/^alias convert_ipynb2py/d' $VIRTUALENV_ACTIVATE_FILE

echo "$BASH_ALIASES_STR" >> $VIRTUALENV_ACTIVATE_FILE
echo "set the alises: convert_py2ipynb and convert_ipynb2py"
source $VIRTUALENV_ACTIVATE_FILE
