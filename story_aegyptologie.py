# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
import matplotlib.pyplot as plt
import tei_reader

# +
# importiere die Bibliothek os, die uns Zugriff auf Funktionen des Betriebs-
# systems (Operating System) gibt
import os

# das ist der Pfad zu dem Verzeichnis, im dem alle koptischen Texte liegen
path_copt = "/home/jana/DaLeLe/uebungen/daten/aegyptologie/corpora-master/bible/sahidica.mark_TEI/koptisch"

# mit der Funktion "listdir()" lassen sich alle in einem Verzeichnis enthaltenen
# Dateien auflisten. Wir listen also 
filenames_copt = os.listdir(path_copt)

# da wir die Dateien in der richtigen Reihe einlesen wollen, sortieren wir
# diese Liste noch
filenames_copt.sort()
print(filenames_copt)

# +
# um XML-Dateien lesen zu können, benötigen wir einen sog. "Parser", der
# das spezielle Format versteht. Das erledigt die Bibliothek "tei_reader" für
# uns, die auf TEI-XML-Dateien spezialisiert sind.

# hier erzeugen wir ein "TeiReader" Objekt, das die Funktion "read_file()"
# besitzt. Diesem Objekt können wir Pfade zu TEI-XML-Dateien geben, die
# es dann für uns einliest und uns Zugriff auf den enthaltenen Text gibt
reader = tei_reader.TeiReader()

# in dieser Liste werden wir die koptischen Kapitel speichern
chapters_copt = []

# jetzt iterieren wir über alle Dateinamen (Kapitel) im koptischen 
# Unterverzeichnis und lesen jede der 16 Dateien ein
for fname in filenames_copt:
    # dies ist der gesamte Pfad zur jeweiligen XML-Datei
    path_to_file = os.path.join(path_copt, fname)
    # lies mit Hilfe der read_file()-Funktion die XML-Datei ein
    corpus = reader.read_file(path_to_file)
    # das Attribut "text" der Variable "corpus" beinhaltet 
    # den koptischen Text, der in der XML-Datei gespeichert ist
    single_text = corpus.text
    # wir fügen den Text des geladenen Kapitels der Variablen hinzu,
    # in der wir den gesamten koptischen Text des Markus-Evangeliums
    # speichern
    chapters_copt.append(single_text)
    
# importiere pandas und erzeuge ein DataFrame mit den Kapiteln
import pandas as pd
chapters_copt = pd.DataFrame({'chapter':chapters_copt})

# zähle die Anzahl der Zeichen je Kapitel und füge sie in einer
# neuen Spalte dem DataFrame hinzu
char_count = [len(chap) for chap in chapters_copt['chapter']]
chapters_copt['char_count'] = char_count
chapters_copt.head()

# +
# das ist der Pfad zu dem Verzeichnis, im dem alle englischen Texte liegen
path_eng = "/home/jana/DaLeLe/uebungen/daten/aegyptologie/corpora-master/bible/sahidica.mark_TEI/englisch"

# alle Dateien im Verzeichnis auflisten und sortieren
filenames_eng = os.listdir(path_eng)
filenames_eng.sort()
print(filenames_eng)

# +
# in dieser Liste werden wir die englischen Kapitel speichern
chapters_eng = []

# jetzt iterieren wir über alle Dateinamen (Kapitel) im englischen 
# Unterverzeichnis und lesen jede der 16 Dateien ein
for fname in filenames_eng:
    # dies ist der gesamte Pfad zur jeweiligen XML-Datei
    path_to_file = os.path.join(path_eng, fname)
    # lies mit Hilfe der read_file()-Funktion die XML-Datei ein
    file_handle = open(path_to_file)
    # das Attribut "text" der Variable "corpus" beinhaltet 
    # den koptischen Text, der in der XML-Datei gespeichert ist
    single_text = file_handle.read()
    # wir fügen den Text des geladenen Kapitels der Variablen hinzu,
    # in der wir den gesamten koptischen Text des Markus-Evangeliums
    # speichern
    chapters_eng.append(single_text)
    
chapters_eng = pd.DataFrame({'chapter':chapters_eng})

# zähle die Anzahl der Zeichen je Kapitel und füge sie in einer
# neuen Spalte dem DataFrame hinzu
char_count = [len(chap) for chap in chapters_eng['chapter']]
chapters_eng['char_count'] = char_count
chapters_eng.head()
# -

plt.hist(chapters_copt['char_count'], alpha=0.5, range=[1000,9000], label='coptic')
plt.hist(chapters_eng['char_count'], alpha=0.5, range=[1000,9000], label='english')
plt.legend()

# ## Unicode exploration

# [Hier](https://de.wikipedia.org/wiki/Unicode) wird beschrieben, was Unicode ist. 

mark01_signs = set(mark01.text)
mark01_signs = list(mark01_signs)
mark01_signs.sort()
print(mark01_signs)

type(mark01_signs[0])

print(signs[7].encode('raw_unicode_escape'))

print('\u03e9')

# remove non-coptic symbols
non_coptic = ['\n', ' ', '.', ',', ';', '[', ']']
mark01_signs = [sign for sign in mark01_signs if sign not in non_coptic]
mark01_signs

# +
# make a unicode table
glyph_table = pd.DataFrame()
glyph_table['glyph'] = [sign for sign in mark01_signs]
glyph_table['unicode'] = [sign.encode('raw_unicode_escape') for\
                    sign in mark01_signs]

glyph_table.head(30)
# -

# ## Fragen
# * Lade auch die Texte für Markus 2 und Markus 3
# * Wieviele verschiedene Zeichen enthalten diese Texte? Wie lang sind sie?
# * Welche Zeichen kommen in Markus 1, 2 und 3 jeweils nicht vor?

mark02, mark03 = [reader.read_file(join(path, fname)) \
                  for fname in ['Mark_02.xml', 'Mark_03.xml']]


def find_signs(text):
    # list of frequently found non-coptic symbols
    non_coptic = ['\n', ' ', '.', ',', ';', '[', ']']
    sign_list = set(text)
    sign_list = list(sign_list)
    sign_list = [sign for sign in sign_list if sign not in non_coptic]
    sign_list.sort()
    return sign_list


# +
print('Markus 1 enthält {} verschiedene koptische Symbole'\
      .format(len(mark01_signs)))

mark02_signs = find_signs(mark02.text)
print('Markus 2 enthält {} verschiedene koptische Symbole'\
      .format(len(mark02_signs)))

mark03_signs = find_signs(mark03.text)
print('Markus 3 enthält {} verschiedene koptische Symbole'\
      .format(len(mark03_signs)))

# +
all_signs = mark01_signs.copy()
all_signs.extend(mark02_signs)
all_signs.extend(mark03_signs)

all_signs = set(all_signs)

for sign in all_signs:
    if (sign not in mark01_signs) or\
        (sign not in mark02_signs) or\
        (sign not in mark03_signs):
        print(sign)
# -

# ## Text Statistik

# +
# make a DataFrame with all the lines and some Statistics
lines = corpora.text.split('\n')
lines = pd.DataFrame({'line':lines, \
                    'character_count':[len(line) for line in lines]})
number_of_words = [len(line.split(' ')) for line in lines['line']]
lines['word_count'] = number_of_words

lines.head()

# +
# Lines are probably not the right way to represent text
# switch to sentences instead:
# need to get rid of "\n" characters
sentences = corpora.text.replace('\n', '')
sentences = sentences.split('.')

# now we remove all other non-coptic symbols 
# except for spaces from the sentences
# we also remove leading and trailing whitespaces
# and double whitespaces
non_coptic = ['\n', '.', ',', ';', '[', ']', '  ']
for i, sen in enumerate(sentences):
    clean_sen = sen
    for sign in non_coptic:
        clean_sen = clean_sen.replace(sign, '')
    clean_sen = clean_sen.strip()
    sentences[i] = clean_sen

# +
# make a DataFrame with all the Words and some statistics
sentences = pd.DataFrame({'sentence':sentences, \
                    'character_count':[len(sen) for sen in sentences]})
number_of_words = [len(sen.split(' ')) for sen in sentences['sentence']]
sentences['word_count'] = number_of_words

sentences.head()

# +
# make a list of all the words in all the sentences
words = []
for sen in sentences['sentence']:
    temp_words = sen.split(' ')
    words.extend(temp_words)

# apparently, some empty strings still survived our cleaning efforts
print('' in words)

# let's exterminate them:
words = [w for w in words if w != '']
print('' in words)
# -

words = pd.DataFrame({'word':words, \
                    'character_count':[len(w) for w in words]})
words.head()

# +
# number of unique words
unique_word_count = len(words.word.unique())
print('found {} unique words'.format(unique_word_count))

# length of words
unique_word_length = len(words.character_count.unique())
print('found {} unique word lengths'.format(unique_word_length))
print(words.character_count.unique())

# +
# what is the longest word?
for index, row in words.iterrows():
    if row['character_count'] == words['character_count'].max():
        print('the longest word is {}'.format(row['word']))
        
# what does it mean?
# -

# the most common word is
words.word.mode()
# what does it mean?

# ## Some visualizations

maximum_length = words['character_count'].max()
counts, pos, patches = plt.hist(words['character_count'], bins=maximum_length-1, rwidth=0.8,\
        align='left')
plt.xticks(range(1,maximum_length));

bins

# +
# make a pie
labels = range(1, 13)
sizes = [counts[i] / counts.sum() * 100 for i in range(12)]

plt.pie(sizes, labels=labels, autopct='%1.1f%%')
plt.axis('equal');

# +
# make the pie prettier
labels = ['1', '2', '3', '4', '5', '>=6']
sizes = [counts[i] / counts.sum() * 100 for i in range(5)]
sizes.append(counts[5:].sum() / counts.sum() * 100)

plt.pie(sizes, labels=labels, autopct='%1.1f%%')
plt.axis('equal');
# -

# character count over text length:
plt.plot(words['character_count'])
plt.xlabel('words')
plt.ylabel('characters')

# character count over text length:
plt.plot(sentences['word_count'])
plt.xlabel('sentences')
plt.ylabel('words')

# huh! there are some REALLY long sentences there!
# lets find out, what they are!
for index, row in sentences.iterrows():
    if row['word_count'] > 50:
        print('sentence {}'.format(index + 1))
        print(row['sentence'])
        print()

# ## Vergleich mit Übersetzungen

# Quelle für Übersetzungen https://newchristianbiblestudy.org/bible/compare/coptic-nt-sahidic/king-james-version/german-luther-1912/mark/4/

# Ok, there is nothing obviously wrong here. Lets take a look into the English translation to find out, if we find a similar pattern.

mark_01_eng_file = open(join(path, 'Mark_01_eng.txt'), 'r')
mark_01_eng = mark_01_eng_file.read()
mark_01_eng

# What do we see here? The sentences are separated by a double line-break ```\n\n``` and always start with a number, which increases by 1 for every sentence.

# +
# create a sentence dataframe by splitting at double-linebreaks
# and removing all numbers
numbers = range(10)
for n in numbers:
    mark_01_eng = mark_01_eng.replace(str(n), '')
    
# and we remove all the punctuation
punctuation = ['.', ':', ',', ';']
for p in punctuation:
    mark_01_eng = mark_01_eng.replace(p, '')
# -

# lets see if we did a good job cleaning the sentences:
chars = set(mark_01_eng)
chars

# nope, we still need to remove some more non-character chars
punctuation.extend(["'", "?"]) # remember to keep ' ' and '\n'
for p in punctuation:
    mark_01_eng = mark_01_eng.replace(p, '')

# +
# now we split the sentences
sentences_eng = mark_01_eng.split('\n\n')

# and we again remove trailing and leading spaces
sentences_eng = [s.strip() for s in sentences_eng]
sentences_eng
# -

sentences_eng = pd.DataFrame({'sentence':sentences_eng,\
                'word_count': [len(sen.split(' ')) for sen in sentences_eng]})
sentences_eng.head()

# +
words_eng = []
for sen in sentences_eng['sentence']:
    tmp_words = sen.split(' ')
    words_eng.extend(tmp_words)
    
words_eng = pd.DataFrame({'word': words_eng,\
                'character_count':[len(w) for w in words_eng]})
words_eng.head()

# +
# number of unique words:
print('number of unique words in the english text: {}'\
      .format(len(words_eng.word.unique())))

print('number of unique words in the coptic text: {}'\
      .format(len(words.word.unique())))
# -

# Behold! We did not think of case sensitivity! The words "The" and "the" are counted as two different unique words - but they are not! Let's revisit upper and lower case words. Is there a function to turn every word into lower case? [There is!](https://stackoverflow.com/questions/6797984/how-do-i-lowercase-a-string-in-python).

lowercase_words = [w.lower() for w in words_eng['word']]
words_eng['word'] = lowercase_words
words_eng.head()

# how many unique words do we have now?
print('number of unique words in the english text: {}'\
      .format(len(words_eng.word.unique())))

# Now we obviously also need to do this for the coptic text. For this, we need some domain knowledge: what are the lowercase and uppercase versions of the same symbol?

glyph_table.head(50)

print('\u03e5')

lowercase_words = [w.lower() for w in words['word']]
words['word'] = lowercase_words
words.head()

len(words.word.unique())

# +
maximum_length = words['character_count'].max()
plt.hist(words['character_count'], range=[1,maximum_length-1], rwidth=0.8,\
        align='left', alpha=0.5, label='coptic')

plt.hist(words_eng['character_count'], range=[1,maximum_length-1], rwidth=0.8,\
        align='left', alpha=0.5, label='english')
plt.xticks(range(1,maximum_length))

plt.xlabel('word length / characters')
plt.ylabel('count')
plt.legend()
# -

words_eng['word'].mode()

words['word'].mode()

# ## Old: Hyroglyphs

texts = {label: text for label, text in \
         zip(['coptic', 'english', 'hyroglyphs'], corpora.text.split('\n'))}

# +
# line separation seems to be done by large amount of white spaces
# is it always the same number of spaces?

space_list = []
spaces = 0
for char in texts['coptic']:
    if char == ' ':
        spaces += 1
    else:
        space_list.append(spaces)
        spaces = 0
# -

# apparently, the format uses 16 spaces to separate lines
# sometimes there seem to be two spaces to separate words. 
# Does this have a meaning or is this an encoding mistake?
plt.hist(space_list)

# +
line_sep = ' ' * 16 # multiply the space-character by 16
lines = texts['coptic'].split(line_sep)

# for now we assume that double-spaces are mistages and replace
# them by single spaces
for i, line in enumerate(lines):
    lines[i] = line.replace('  ', ' ')

# +
# check, if the cleaning worked:
# line separation seems to be done by large amount of white spaces
# is it always the same number of spaces?

space_list = []
spaces = 0
for line in lines:
    for char in line:
        if char == ' ':
            spaces += 1
        else:
            space_list.append(spaces)
            spaces = 0
            

plt.hist(space_list)
# -

# make a DataFrame from the lines
line_df = pd.DataFrame({'line':lines})

# to count the words now we can split the lines by single spaces
# this would not have been possible if we still had double-spaces
word_count = [len(line.split(' ')) for line in lines]
line_df['word_count'] = word_count # add the word count to the DataFrame

# how many lines are there in total?
len(line_df)

# how many words are there in every line?
plt.hist(line_df['word_count'], bins=20)

# ## What is in the text?
# To understand the structure of the text, we probably first need to gather a bit of domain knowledge to inform our analysis. Following the [description of the coptic language on Wikipedia](https://de.wikipedia.org/wiki/Koptische_Sprache), the language has four major dialects. Which one do we have here?

line_df['line']

# ## Try the same with the English translation, just to sanity-check

space_list_eng = []
spaces = 0
for char in texts['english']:
    if char == ' ':
        spaces += 1
    else:
        space_list_eng.append(spaces)
        spaces = 0

# +
#space_list_eng
# -

plt.hist(space_list_eng)

test_string = 'a    b'
test_string.split('  ')

# # Field sites

# quelle: http://www.ancientlocations.net/
sites = open('/home/jana/DaLeLe/uebungen/daten/aegyptologie/sites.txt')

lines = [line for line in sites]

lines

# +
data_raw = [line for line in lines if '\t' in line]
data_split = []
for i in range(len(data)):
    temp = data[i]
    temp = temp.replace('\t\t', '\t')
    temp = temp.strip('\n')
    data_split.append(temp.split('\t')[-3:-1])
    
data_split
# -

for entry in data_split:
    if len(entry) != 3:
        print(entry)

# +
df = pd.DataFrame()
df['site_name'] = [entry[0] for entry in data_split]

north = []
east = []

for entry in data_split:
    N, E = entry[1].split(', ')
    N = float(N.split(' ')[0])
    E = float(E.split(' ')[0])
    north.append(N)
    east.append(E)
    
df['north'] = north
df['east'] = east
# -

df.head()

df.to_csv('/home/jana/DaLeLe/uebungen/daten/aegyptologie/sites.csv')

# ### Idea:
# Plot site locations over map of Egypt following https://matplotlib.org/basemap/users/examples.html

!
