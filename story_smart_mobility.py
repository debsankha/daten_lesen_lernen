# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import pandas as pd
from os.path import join
import matplotlib.pyplot as plt

# source: https://zenodo.org/record/820576#.XNHYdN9fhhE
# hackathon: https://scottproject.eu/2019/01/10/vehicle-data-hackathon/
path = '/home/jana/DaLeLe/uebungen/daten/smart-mobility'
acc = pd.read_csv(join(path,'accelerations.csv'))
beagle = pd.read_csv(join(path,'beaglebones.csv'))
gyro = pd.read_csv(join(path,'gyroscopes.csv'))
obd = pd.read_csv(join(path,'obdData.csv'))
pos = pd.read_csv(join(path,'positions.csv'))
trips = pd.read_csv(join(path,'trips.csv'))

acc.head()

acc_trip = acc[acc['trip_id'] == 4]
plt.plot(acc_trip['x_value'], alpha=0.5, label='x')
plt.plot(acc_trip['y_value'], alpha=0.5, label='y')
plt.plot(acc_trip['z_value'], alpha=0.5, label='z')
#plt.ylim(0.9,1)
plt.legend()

acc_trip.x_value.unique()

beagle.head()

gyro.head()

# 04 calculated engine load
# 05 engine cooling temp
# 0B intake manifold absolute pressure
# 0C engine rounds per minute
# 0D speed [km/h]
# 0F intake air temperature
# 10 MAF airflow rate
# 11 throttle position
# 33 absolute barometric pressure [k Pa]
# 3C catalyst temperature
obd.head(22)

obd.trip_id.unique()

speed = obd[(obd['obdPid'] == '0D') & (obd['trip_id'] == 33)]
throttle_pos = obd[(obd['obdPid'] == '0C') & (obd['trip_id'] == 33)]

# +
fig, ax = plt.subplots()
ax.plot(speed['data'], label='speed')
ax2 = ax.twinx()
ax2.plot(throttle_pos['data'], label='rpm',color='red')
ax.legend(loc=2)
ax2.legend(loc=1)
ax.set_ylabel('speed / km/h')
ax2.set_ylabel('rpm')

#ax.set_xticks([])
# -

plt.plot(throttle_pos['data'], label='throttle')

pos.head()

trips.head()


