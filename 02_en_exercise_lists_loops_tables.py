# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Exercise 2
# ### Some tips
# $\rightarrow$ You should take a look at the exercises at home and try to solve as many tasks as you can. In the tutorials you could go on with solving the tasks you could not finish at home and if necessary, ask for help from the tutors. 
#
# $\rightarrow$ You you encounter an error:
# 1. _Read_ and _understand_ the error message. 
# 2. Try to find out the solution to the problem ($\rightarrow$ this may be hard at the beginning, but it is very educational!)
# 3. Search using a search engine for the problem (hint: Stackoverflow) or ask your neighbour!
# 4. Ask the tutors.
#
# $\rightarrow$ Look out for the sign <font color='green'>**HINT**</font>: these are hints that helps you with solving the task and sometimes additional information regarding the task. 
#
# $\rightarrow$ Tasks marked **(Optional)** are for those of you who are extra fast :-).

# ### Lists, loops and tables
# 1. **Lists and loops**  
#   1. Create a list cnsisting of words (strings) that make up a sentence.
#   2. By iterating over the list with a ```for``` loop, print each word.
#   3. By iterating over the list with a ```for``` loop, add all the strings in the list together into a longer string.  
#       <font color='green'>**HINT**: You need to create a variable before the ```for``` loop, where the total string will be stored.</font>
#   4. Create a list with integers from 1 to 10 (including both 1 and 10). Print each element of the list. 
#   5. Print only the odd numbers of the list, then print only the even numbers. Print only the first 4 elements, then only the last four. 
# 2. **Tables**
#   1. Read the table "titanic.csv" using the function ```read_csv()``` from the python library ```Pandas``` into a DataFrame. <font color='green'>**HINT:** You can download the "titanic.csv" file from the StudIP folder for this exercise (02 Übung). Then you need to upload the .csv file to JupyterHub using the ```Upload``` button (in the dashboard direct beside ```new```). It is advisable that you create a new folder called `data` and this data (alongwith all the datasets you will use in future) in that folder. To import a library (e.g. pandas), you have to import it using the `import` keyword.</font>
#   2. Display the first few rows in the table. Find out how many rows the table has. <font color='green'>**HINWEIS:** Use the funktions ```head()``` und ```len()```.</font>
#   3. Find out what each column of the table means. The column ```age``` is straightforward, but what does the ```sibsp``` column contain?
#   4. Compute the average ticket price the passengers paid.
#   5. **(Optional):** How many passengers survived?
#   6. **(Optional):** Create a list containing integers between 0 und 100 using the ```range()``` funktion and the ```list()``` function. Print the length of the list (use ```len()```). <font color='green'>**HINT**: When creating lists in this way, you _need_ to use the ```list()``` function on the object created by the ```range()``` function. This is because ```range()``` creates only a so-called "Generator", that then _can_ be converted into a list. Subsequent application of the ```list()``` function will convert the generator to a proper list. You can check that all these are happening by inspecting the result of each function using the ```type()``` function.</font> 

# +
generator = range(0,5)
print(type(generator))

new_list = list(generator)
print(type(new_list))
# -

# <font color='green'>This behaviour is not so important right now, we mention it here for sake of completeness.</font>
#
# G. **(Optional)** Create a list with 5 numbers of choice. Iterate over the list and add to each element, the sum of all the elements in the list. Store the result in a new list.  
#     <font color='green'>**HINT**: The function ```sum()``` automatically computes the sum of a list. Creating a new empty list and appending elements at the end of it works as follows:</font>

# +
# HINT
# a new list creation
new_list = []
print(new_list)

# appending an element to the new list
new_list.append(2)
print(new_list)
# -

# H. **(Optional)** Set every second element of the list in task F to zero, using a slice-operator.  
#
#   <font color='green'>**HINT 1**: A single element of a list can be modified as follows:</font>

# +
# create a new list and print it
test_list = [1,2,3,4,5]
print(test_list)

# set the element at index 2 to 100
test_list[2] = 100
print(test_list)
# -

# <font color='green'>**HINT 2**: A list with repeated elements can be created as follows:</font>

# +
# "multiplying"  a list with a whole number n creates a
# list with the original list repeated n times

# a list with a single element repeated 5 times
new_list = [0] * 5 
print(new_list)

# a list with two elements repeated 3 times
new_list = [1, 2] * 3 
print(new_list)

# a list with 5 elements repeated twice
new_list = [1, 2, 3, 4, 5] * 2
print(new_list)
