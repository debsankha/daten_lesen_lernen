# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 07 - Allgemein: Hashtags
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
# Im folgenden Tutorial wollen wir etwas mehr über das Twitter-Verhalten der verschiedenen user-Gruppen herausfinden. Wieviele Hashtags und Links benutzen sie? Wie lang sind die Worte, die sie benutzen? 
# Das Tutorial gliedert sich in
# * [Mittelwert, Median und Standardabweichung](#mittelwert_und_median)
# * [Hashtags](#hashtags)

# <a name="mittelwert_und_median"></a>1. Mittelwert, Median und Standardabweichung
# ---
# Zur Vorbereitung laden wir wieder die Datensätze, die wir gespeichert haben. Da insbesondere der Datensatz der normalen User so groß ist (192 MB), dass er unserer IT-Infrastruktur Probleme bereitet, haben wir zwei neue Versionen des normalen- und Troll-user Datensatzes bereitgestellt, die nur eine Untermenge der verfügbaren Tweets enthalten. Die Tweets in den neuen Versionen haben wir zufällig aus dem ganzen Datensatz gezogen. Falls dich interessiert, wie das gemacht wird, kannst du die Vorgehensweise im (ebenfalls im StudIP hochgeladenen) Notebook ```data_subsampling.ipynb``` nachsehen.
#
# **A.** lade die zwei Datensätze ```tweets_normal_subset.csv``` und ```tweets_trump_subset.csv``` aus dem StudIP herunter und in den JupyteHub hoch. Wir laden die Tabellen wieder in ```pandas``` DataFrames:

# +
# importiere die Bibliothek Pandas mit dem Kürzel "pd"
import pandas as pd

tweets_trump = pd.read_csv('tweets_trump.csv')
tweets_troll = pd.read_csv('tweets_troll_subset.csv')
tweets_normal = pd.read_csv('tweets_normal_subset.csv', encoding='ISO-8859-1')
# -

tweets_troll.head()


# Mit Hilfe von einfachen statistischen Maßen wie Mittelwert und Median lässt sich schnell ein Überblick über die in einem Datensatz enthaltene Information gewinnen:  
#
# **B.** Berechne Mittelwert, Median und Standardabweichung der Wortanzahl der Tweets der verschiedenen Datensätze.  
#
# **C.** Was bedeutet die Standardabweichung in diesem Kontext? Wie interpretierst du die Ergebnisse?  
#
# **D.** Um die Robustheit von Mittelwert und Median zu testen, setze den Wert für die Tweet-Länge in der ersten Zeile des DataFrames der Trump-Tweets auf $10^6$ (eine Million). Wie verhält sich der Mittelwert und wie der Median? Wie die Standardabweichung? <font color='green'>**HINWEIS:** Du solltest für diese Aufgabe eine Kopie des DataFrames (siehe ```df.copy()```-[Funktion](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.copy.html) erstellen, da es gute Praxis ist, keine Originaldaten zu manipulieren sondern Kopien zu erstellen, wenn Originaldaten verändert werden sollen. </font>  
#
# **E. (optional)** Wie kannst du dir mit Hilfe einer Visualisierung schnell Überblick über die Werte in einem Datensatz erhalten und "Außreißer, wie der den wir in Aufgabe **D.** eingefügt haben identifizieren?

# <a name="Hashtags"></a>2. Hashtags
# ---
# Für den folgenden Aufgabenteil schreiben wir uns eine kleine Funktion, die herausfindet, ob ein Wort ein Hashtag ist oder nicht.

def isHashTag(word):
    if '#' in word:
        return True
    return False


# **A.** Versuche zu verstehen, was die Funktion ```isHashTag()``` tut und füge Kommentare, die die Funktion erklären in die obenstehende Code-Zelle, in der die Funktion definiert wird, ein. Teste die Funktionsweise der Funktion anhand einiger Worte (Strings). 
#
# **B.** Der Datensatz mit den Troll-Tweets enthält auch andere Sprachen als Englisch. Für die folgende Analyse ist es wichtig, dass wir nur die Englischen Tweets betrachten, da andere Sprachen die Ergebnisse verzerren würden. Deswegen: filtere denn Troll-Datensatz so, dass nur die Englischen Tweets übrig bleiben. <font color='green'>**HINWEIS:** Du kannst dafür die in der Spalte ```Language``` enthaltene Information verwenden. </font>.
#
# **C.** Für jeden der drei Datensätze: iteriere über alle Tweets, zerteile jeden Tweet in eine Liste von Worten. Finde in dieser Liste die Hashtags. Speichere sowohl die Anzahl der Hashtags pro Tweet als auch die einzelnen Hashtags in einer (separaten) Liste.  Füge die Anzahl der Hashtags als neue Spalte ```number_hashtags``` zu den jeweiligen DataFrames hinzu. Speichere die DataFrames am Ende der Aufgabe ab. <font color='green'>**HINWEIS:** Wenn du eine existierende Liste um eine Teil-Liste erweitern möchtest, kannst du die Funktion ```existierende_liste.extend(teil_liste)``` verwenden. </font>. Für einen einzelnen Tweet funktioniert die oben beschriebene Vorgehensweise wie folgt:

sample_tweet = '#JobOpportunity: als Studi bei uns an der @subugoe in der Abt. #Forschung & #Entwickung in der #Paulinerkirche mitarbeiten. Reguläres HiWi-Entgelt.  Aufgaben von Büro bis Veranstaltungen. Alle Deine Skills sind interessant für uns; außer Kaffeekochen, das macht die Maschine'
words = sample_tweet.split(' ')
hashtags = [word for word in words if isHashTag(word)]
number_hashtags = len(hashtags)
print(hashtags)
print('number of hashtags: {}'.format(number_hashtags))

# **D.** Was ist die mittlere Anzahl an verwendeten Hashtags pro Tweet für die drei Datensätze? Was die Standardabweichung? Unterscheiden sich die drei User (Gruppen) stark? Kannst du dir das erklären?
#
# **E.** Erstelle für jeden der drei Datensätze ein neues DataFrame, das die Hashtags und ihre Länge (Zeichenanzahl) enthält. Spreichere die DataFrames lokal ab.
#
# **F.** Wieviele verschiedene Hashtags benutzen Trump, die Trolle und die normalen User jeweils? <font color='green'>**HINWEIS:** Die Funktion ```df['spalte'].unique()``` hilft hier!</font>
#
# **G. (optional)** Visualisiere die Verteilung der Hashtag-Längen der drei Datensätze in einem Histogramm. Unterscheiden sie sich? Lass dir alle Hashtags mit einer Länge größer 30 Zeichen aus dem Trump-Datensatz anzeigen.
#
# **H. (optional)** Suche in den Tweets nach der Anzahl der Links. <font color='green'>**HINWEIS:** mit der Funktion ```str.count()``` eines Strings kannst du die Anzahl der Vorkommnise eines Teil-Strings zählen (siehe die [Dokumentation](https://docs.python.org/2/library/string.html) von ```string.count()```. Du kannst die Aufgabe auch z.B. mit Hilfe einer Funktion wie ```isHashTag()``` wie oben beschrieben lösen.</font> Füge allen drei DataFrames eine weitere Spalte mit der Anzahl an Links je Tweet hinzu. Wieviele Links je Tweet posten Trump, die Trolle und die normalen User durchschnittlich?   
#
# **I. (optional)** Entferne alle Links aus den Tweets und überprüfe, ob unsere Analyse der Hashtag-Längen sich verändert hat. Speichere auch die Hashtag DataFrames nach dem Bereinigen wieder ab.

# [Anfang](#top)
