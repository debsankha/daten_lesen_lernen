# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Übung 07 - Wirtschaftswissenschaften: Mittelwert, Median und Standardabweichung
# ### Praktische Hinweise
# $\rightarrow$ Übungen sind dafür gedacht, dass ihr sie euch daheim anseht und versucht, ein paar Aufgaben selbst zu lösen. In den Tutorien könnt ihr euch weiter mit dem Lösungen der Aufgaben beschäftigen und dabei Hilfe von den Tutor*innen bekommen.  
#
# $\rightarrow$ Wenn ein Fehler auftritt:
# 1. Fehlermeldung _lesen_ und _verstehen_
# 2. Versuchen, selbst eine Lösung zu finden ($\rightarrow$ hier findet das Lernen statt!)
# 3. Das Problem googlen (Stichwort: Stackoverflow) oder den/die Nachbar*in fragen
# 4. Tutor*in fragen
#
# $\rightarrow$ Unter <font color='green'>**HINWEIS**</font> werden Hinweise gegeben, die für die Lösung der Aufgabe hilfreich sind und oftmals auch weiterführende Informationen zur Aufgabe liefern.
#
# $\rightarrow$ Mit **(Optional)** gekennzeichnete Übungsteile sind für besonders schnelle Menschen :-).

# <a name="top"></a>Inhalt
# ---
# In diesem Tutorium berechnen wir ein paar einfache Kenngrößen wie den Mittelwert, Median und die Standardabweichung der Streikanzahl für ein spezifisches Jahr und versuchen, Trends aus dem Datensatz mit politischen Entwicklungen oder historischen Ereignissen zu erklären. 
#
# Das Tutorial gliedert sich in
# * [Mittelwert und Median](#mittelwert_und_median)
# * [Standardabweichung](#standardabweichung)

# <a name="deskriptive_statistik"></a>1. Mittelwert und Median
# ---
# Zur Vorbereitung laden wir die Streik-Tabelle in ein ```DataFrame```:

# +
# die Funktion "join()" brauchen wir, um Pfade im Dateisystem
# sicher zu handhaben
from os.path import join

# die Bibliothek Pandas liefert uns das DataFrame
import pandas as pd

# Dateiname 
fname_streiks = 'NumberofLabourDisputes_Broad.xlsx'

# lade die Datei als pandas DataFrame
streiks = pd.read_excel(join("daten/", fname_streiks), sheet_name='Data Long Format')
# -

# Die mittlere Anzahl an Streiks in einem spezifischen Jahr lässt sich folgendermaßen berechnen:

jahr = 1990
filter_jahr = streiks['year'] == jahr
streiks_jahr = streiks[filter_jahr]['value']
mean = streiks_jahr.mean()
print('Die mittlere Anzahl an Streiks im Jahr {} ist {:1.1f}.'.format(jahr, mean))

# **A.** Berechne die mittlere Anzahl an Streiks über alle Länder (für die Daten vorhanden sind) für die Zeitspanne zwischen 1970 und 2006. Speichere die Ergebnisse in einer Liste. Berechne auch den Median und speichere ihn ebenfalls in einer Liste.   
# **B.** Visualisiere die mittlere Anzahl der Streiks in der oben genannten Zeitspanne als Zeitreihe (wie in Aufgabe **1.B**). Achte auf Titel, Achsenbeschriftungen und Legende.  
# **C.** Visualisisere Verlauf des Mittelwerts und des Medians in der selben Abbildung. Was ist der Unterschied zwischen Mittelwert und Median und wie wirkt er sich auf deine Ergebnisse aus?  
# **D.** Was für einem Trend folgt die mittlere Anzahl der Streiks? Lässt sich der selbe Trend in der Anzahl der streikenden Arbeiter und/oder der Anzahl der gestreikten Tage wiederfinden?  
# **E.** Erstelle ein neues DataFrame, das für jedes Jahr die mittlere Anzahl an Streiks, Streiktagen und streikenden Arbeitern enthält. Speichere das DataFrame.   
# **F.** Visualisiere die Anzahl der Streikenden bzw. die Anzahl der Streiktage pro Streik über die Zeit. Was für Trends kannst du ausmachen?  
# **G. (optional)** Stelle eine Hypothese auf, um die Trends, die du in Aufgabe **2.F** gesehen hast, zu erklären. Was für Daten würdest du benötigen, um diese Hypothese zu überprüfen? Wo könntest du solche Daten finden?  
# **H. (optional)** Beschaffe die Daten aus Aufgabe **2.G** und versuche, sie mit mit den Daten zu den Streiks zusammenzuführen. Versuche, deine Hypothese mit einer Visualisierung zu belegen oder zu widerlegen.

# [Anfang](#top)

# <a name="standardabweichung"></a>2. Standardabweichung
# ---
# Die Standardabweichung der Streiks in einem Spezifischen Jahr lässt sich wie folgt berechnen:

jahr = 1990
filter_jahr = streiks['year'] == jahr
streiks_jahr = streiks[filter_jahr]['value']
std = streiks_jahr.std()
print('Die Standardabweichung der Streiks im Jahr {} ist {:1.1f}.'.format(jahr, std))

# **A.** Was ist die Bedeutung der Standardabweichung für den Streik-Datensatz?  
# **B.** Berechne die Standardabweichung der Streik-Anzahl für den Zeitraum zwischen 1970 und 2006 (wie den Mittelwert in Aufgabe **2.A**) und füge die Werte zum in Aufgabe **2.E** erstellten DataFrame hinzu.  
# **C.** Visualisiere den zeitlichen Verlauf der Standardabweichung in der selben Abbildung wie den Mittelwert und den Median. Was fällt dir auf?  
# **D. (optional)** Berechne Mittelwert und Standardabweichung der Streiks für den Zeitraum 1970 bis 2006, aber diesmal für einzelne Länder (also: was ist die mittlere Anzahl an Streiks in Frankreich im Zeitraum zwischen 1970 und 2006?).  
# **E. (optional)** Welche Länder haben im Zeitraum zwischen 1970 und 2006 mehr als 30 Datenpunkte? Welches dieser Länder hat die niedrigste mittlere Streikanzahl und welches die höchste? Welches Land hat die niedrigste Standardabweichung der Anzahl der Streiks, welches die höchste? Lassen sich diese Ergebnisse mit politischen Entwicklungen erklären? Wenn du ein Unternehmer wärst, was wäre dir wichtiger: eine niedrige mittlere Anzahl an Streiks oder eine niedrige Standardabweichung? Warum?

# [Anfang](#top)
