# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Analyzing the operations of a on-demand ridesharing service
# [Ecobus](https://www.ecobus.jetzt/home.html) was a pilot project that operated a on-demand ridesharing service in some rural areas near Göttingen. Ecobus worked as follows: 
# 1. A customer would *request* a ride from A to B.
# 2. A bus would be sent to pick him/her up.
# 3. So far, it is the same as a online Taxi booking service. Here comes the difference: if two requests have such origin/destination pairs that only one bus can service them both, then only one bus would be sent. An example trajectory of a bus would be: 
#   - Pickup request 1 at location a.
#   - Pickup request 2 at location b.
#   - Dropoff request 2 at location c.  
#   - Dropoff request 1 at location d.
#   The algorithm assigning buses to requests made sure that no passenger has to suffer too much delay because his bus is making a detour to pick up a new customer. 
#   
# ## The dataset description
# Download the dataset from [link](https://owncloud.gwdg.de/index.php/s/Ce8uno1pZ8ZKnza/download). The dataset is a table with the following structure:
#
# |datetime|lat|lon|bus_id|request_id|stop_type|
# |----------|----|---|---|----------|---------|
# |'2019-01-10 06:16:01'|51.23|10.12|'bus_1'|'a5gj9'|'PickUp'|
#
# Each row of this table describes a stop of a bus in the Ecobus vehicle fleet. The columns depict:
#
# |Column| Description|
# |------|------------|
# |datetime | The datetime of the stop |
# |lat, lon | The geographical coordinates of the stop |
# |bus_id | An identifier for the buses |
# |request_id | An unique identifier for the request |
# |stop_type| 'PickUp' if the request `request_id` was picked up at this stop, 'DropOff' if it was dropped off here|

# ## Goal one: analyze temporal properties of the requests
# 1. How many requests were serviced each day? Create a timeseries plot with date in X axis and the request counts on the Y axis.
# 2. Does the number of requests depend on the:
#   - Time of day?
#   - Day of week?
# 3. How long does each customer stay on the bus? Create a histogram. 
# 4. How many customers had at least one co-passenger? Plot the percentage of passangers who had one co-passanger in a pie-chart 

# ## Goal two: analyze spatial properties of the requests
# 1. Draw a scatterplot of the (lon, lat) pairs of pickup locations. Draw a second scatterplot for the dropoff locations. 
# 2. Compute straight-line distances between the pickup and dropoff location of each request. Use the [Haversine formula](https://en.wikipedia.org/wiki/Haversine_formula). Plot the distances in a histogram.
# 3. (optinal) Repeat the previous task, but draw the scatterplot on top of a streetnetwork visualization using [osmnx](https://github.com/gboeing/osmnx).
# 4. (optional) Identify major clusters in the scatterplots of 2 with towns/villages. Use [OpenStreetMap](https://www.openstreetmap.org).
# 5. (optional) Create a table depicting from which town how many people travel to which town. Example:
#
# |From\To|Goslar|Clausthal-Zellerfeld|St. Andreasberg|None|
# |-|------|--------------------|---------------|----|
# |Goslar|23|12|2|20|
# |Clausthal-Zellerfeld|14|20|5|19|
# |St. Andreasberg|18|8|25|9|
# |None|25|18|7|13|
#


