# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Funktion und Handel mit Keramik
#
# Die über 30.000 Fundstücke von Keramik im Mittelmeerraum, die im [ICRATES-Katalog](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/overview.cfm) hinterlegt sind bieten einen reichhaltigen Fundus von Informationen über die damals gebräuchlichen Gefäße. Darüber hinaus ist der Katalog angereichert mit Fundorten und Informationen zum Gefäßtyp- und Funktion. In den Übungen haben wir uns schon damit beschäftigt, die Fundorte und Gefäßgröße zu analysieren. In diesem Projekt soll es darum gehen, diese Analyse weiterzuführen und selbstständig Erkenntnisse über die Gefäßfunktion sowie die Handelsnetzwerke, in denen diese Gefäße transportiert und gehandelt wurden zu gewinnen.
#   
# ## Der Datensatz
# Der Datensatz besteht aus dem Katalog mit Informationen über jedes einzelne Fundstück, der mit sechs weiteren Tabellen über IDs verknüft ist, die Informationen zu verschiedenen Aspekten der Fundstücke bereitstellen:
# * ```ICRATES_LOCATION``` beinhaltet Informationen zum Fundort
# * ```ICRATES_PUBLICATION``` beinhaltet Informationen über die Publikationen, in denen Informationen über die Fundstücke publiziert wurden.
# * ```ICRATES_DEPOSIT``` beinhaltet Informationen über die Lagerstätte, in denen die Fundstücke gefunden wurden.
# * ```ICRATES_STANDARD_FORM``` beinhaltet typologische und chronologische Informationen zu den am häufigsten im Katalog vorkommenden Gefäßtypen.
# * ```ICRATES_OCK``` und ```ICRATES_LRP``` beinhaltet Informationen über die auf den Gefäßen vorhandenen Prägungen.
#
# Die verschiedenen Datensätze sind mit IDs folgendermaßen verknüpft:   
# ![](https://archaeologydataservice.ac.uk/archives/view/icrates_lt_2018/images/ICRATES_ER.png)

# ## Aufgaben
# 1. Die Spalte ```Function``` des Kataloges enthält Informationen über die Funktion des Gefäßes. 
#     * Stelle die in der Spalte enthaltenen Informationen graphisch z.B. mit Hilfe von bar-charts dar. 
#     * Für wieviele Fundstücke sind Informationen über die Funktion bekannt? 
#     * Gibt es einen Zusammenhang zwischen anderen in der Tabelle enthaltenen Informationen (z.B. ```Publication_ID```) und dem Vorhandensein von Informationen über die Funktion der Gefäße?
# 2. In den Übungen haben wir die Gefäßgröße untersucht. 
#     * Finde heraus, ob es einen Zusammenhang zwischen Gefäßfunktion und Gefäßgröße gibt. 
#     * Stelle deine Ergebnisse graphisch dar. Dazu eignet sich z.b. ein [boxplot](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.boxplot.html).
# 3. In ther Spalte ```Specific_Shape``` ist ebenfalls Information über die Funktion enthalten (z.B. "Teller"). 
#     * Wieviele verschiedene Einträge gibt es hier? 
#     * Lassen sich diese den in der Spalte ```Function``` enthaltenen Funktionen zuordnen? 
#     * Gibt es einen Zusammenhang zur Größe?
# 4. Die Spalte ```Fabric``` gibt Informationen über den Typ (Produktionsort).
#     * Finde heraus, wo Gefäße eines bestimmten Typs gefunden wurden. Dies kann z.B. Hinweise über die Handelsrouten, auf denen die Gefäße transportiert wurden liefern.
#     * Stelle graphisch dar, aus welchen Typen sich die Fundstücke an den 10 größten Fundorten zusammensezten. Dafür kannst du z.B. eine Darstellung von [pie-charts](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.pie.html) auf einer Landkarte wählen. 
