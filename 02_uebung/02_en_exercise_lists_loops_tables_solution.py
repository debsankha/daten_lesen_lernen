# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _This teaching material can be freely used, distributed and modified as per the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) license._   
#  _Authors: Jana Lasser (jana.lasser@ds.mpg.de), Debsankha Manik (debsankha.manik@ds.mpg.de)._  
#  _This teaching material is created for the project "Daten Lesen Lernen", funded by Stifterverband and the Heinz Nixdorf Stiftung._

# # Solutions to Exercise 2: Lists, loops and tables

# ### Lists and loops

# 1.A
word_list = ['A', 'string', 'is', 'a', 'chain', 'of', 'characters']

# 1.B
for word in word_list:
    print(word)

# +
# 1.C
sentence = '' # this is an empty list

for word in word_list:
    sentence = sentence + word
    
print(sentence)

# +
# 1.C second try:
sentence = '' # this is an empty list

for word in word_list:
    # add an extra space before each word
    sentence = sentence + ' ' + word
    
print(sentence)

# +
# 1.D 
numbers = [1,2,3,4,5,6,7,8,9,10]

for number in numbers:
    print(number)

# +
# 1.E
# even numbers
print(numbers[1::2])

# odd numbers
print(numbers[0::2])

# the first four elements
print(numbers[0:4])

# the last four elements
print(numbers[-4:])
# -

# ### Tables

# +
# 2.A
# import the library pandas under the 
# alias "pd"
import pandas as pd

# load the Titanic dataset and save as
# variable 'titanic_data' 
# DataFrame in der Variable "titanic_data"
titanic_data = pd.read_csv('daten/titanic.csv')

# +
# 2.B
# Show the first few rows of the table
titanic_data.head()

length_data = len(titanic_data)
print('The dataset has {} rows.'.format(length_data))
# -

# 2.C
#
# This task is more of an exploration task than a task with a single answer, but this is quite often the case when one needs to deal with datasets and understand them. Some background information about the titanic dataset is [available here](https://www.kaggle.com/francksylla/titanic-machine-learning-from-disaster). We summarize the information for each column here as well:
#   * ```sibsp``` stands for "sibling" or "spouse" (indicates whether a passenger had a sibling or spouse on board).
#   * ```parch``` stands for "parent" or "child" (indicates whether a passenger had a parent or child on board).
#   * ```alone``` is self explanatory (equivalent to both ```sibsp``` and ```parch``` being zero).
#   * ```pclass``` or ```class``` indicates the class in which a passenger travelled (once as a number, then as a string).
#   * ```fare``` is the price one paid for the ticket, ```embark town``` means the town at which the passenger boarded the ship. ```embarked```  is an abbriviation for ```embark town```.
#   * ```sex``` and ```age``` are self-explnatory.
#   * ```survived``` und ```alive``` indicates whether a passenger survived or not (once as a number, then as a string).
#   * ```deck``` indicates the deck at which a passenger's berth at Titanic was. 
#   * ```NaN``` as a cell value indicates that no information is avilable regarding certain column for a certain row.

# 2.D 
# compute the average of the column 'fare'
mean_price = titanic_data['fare'].mean()
print('The average ticket price is {}.'\
      .format(mean_price))

# 2.E Optional 
# the number of survivors can be computed by summing
# over the column 'survived'. This works, because the
# survivors have value 1 and the non-survivors have
# value 0
survivors = titanic_data['survived'].sum()
print('{} people survived the Titanic disaster.'\
     .format(survivors))

# 2.F
long_list = list(range(0,100))
list_length = len(long_list)
print(list_length)

# +
# 2.G Optional
numbers_list = [7, 104, 79, 23, 56]
sum_list = [] # dies ist eine leere Liste

for num in numbers_list:
    # add to each number the sum of
    # the whole list
    num = num + sum(numbers_list)
    
    # print the new number
    print('new number: {}'.format(num))
    
    # append the new number to the sum_list
    sum_list.append(zahl)
    
print(sum_list)
# -

# 2.H
long_list[0::2] = [0]*50
print(long_list)


