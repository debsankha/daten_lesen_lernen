# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# _Lizenz: Das folgende Lehrmaterial kann unter einer [CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) Lizenz frei verwendet, verbreitet und modifiziert werden._   
#  _Authoren: Jana Lasser (jana.lasser@ds.mpg.de)_  
#  _Das Lehrmaterial wurde im Zuge des Projektes "Daten Lesen Lernen", gefördert vom Stifterverband und der Heinz Nixdorf Stiftung erstellt._

# # Lösungen zu Übung 2: Listen, Loops und Tabellen

# ### Listen und Loops

# 1.A
wort_liste = ['Ein', 'string', 'ist', 'eine',\
             'Kette', 'von', 'Zeichen']

# 1.B
for wort in wort_liste:
    print(wort)

# +
# 1.C
satz = '' # das ist ein leerer string

for wort in wort_liste:
    satz = satz + wort
    
print(satz)

# +
# 1.C zweiter Anlauf:
satz = ''

for wort in wort_liste:
    # addiere ein Leerzeichen vor jedem Wort
    satz = satz + ' ' + wort 
    
# addiere einen Punkt am Ende des Satzes
satz = satz + '.'

print(satz)

# +
# 1.D 
zahlen = [1,2,3,4,5,6,7,8,9,10]

for zahl in zahlen:
    print(zahl)

# +
# 1.E
# gerade Zahlen
print(zahlen[1::2])

# ungerade Zahlen
print(zahlen[0::2])

# die ersten vier Elemente
print(zahlen[0:4])

# die letzten vier Elemente
print(zahlen[-4:])
# -

# ### Tabellen

# +
# 2.A
# importiere die Bibliothek Pandas unter dem
# Kürzel "pd"
import pandas as pd

# lade den Datensatz und speichere das resultierende
# DataFrame in der Variable "titanic_data"
titanic_data = pd.read_csv('daten/titanic.csv')

# +
# 2.B
# zeige die ersten paar Zeilen des Datensatzes an
titanic_data.head()

length_data = len(titanic_data)
print('Der Datensatz hat {} Einträge.'.format(length_data))
# -

# 2.C
#
# Diese Aufgabe ist mehr eine Recherche-Aufgabe, aber auch das ist sehr oft Teil der Arbeit, wenn man den Inhalt eines Datensatzes verstehen will. Informationen über den Datensatz "titanic" finden sich nach kurzem Googlen z.B. [hier](https://www.kaggle.com/francksylla/titanic-machine-learning-from-disaster). Wir tragen also Informationen über die einzelnen Spalten zusammen:
#   * ```sibsp``` steht für "sibling" oder "spouse" (Geschwister, Ehepartner) und ist ein Indikator dafür, ob ein\*e Passagier\*in Verwandte auf dem Schiff hatte.
#   * ```parch``` steht für "parent" oder "child" (Eltern, Kinder) und ist ein Indikator dafür, ob ein\*e Passagier\*in Elternteil oder Kind von einer/einem anderen Passagier\*in am Schiff waren.
#   * ```alone``` ist entsprechend ein Indikator dafür, ob eine Person allein unterwegs war (sowohl ```sibsp``` als auch ```parch``` sind null).
#   * ```pclass``` oder ```class``` stehen für die Klasse, in der die Person gereist ist (einmal als Zahl, einmal als Wort).
#   * ```fare``` ist der Kaufpreis des Tickets, ```embark town``` steht für die Stadt, an der die Person zugestiegen ist und ```embarked``` ist ein Kürzel für die ```embark town```.
#   * ```sex``` und ```age``` sind Aussagen über Geschlecht und Alter der Person.
#   * ```survived``` und ```alive``` sagen aus, ob die Person das Unglück überlebt hat (einmal als Zahl, einmal als Wort).
#   * ```deck``` steht für das Deck, auf dem die Person auf der Titanic untergebracht war. 
#   * ```NaN``` wird als Eintrag benutzt, falls keine Information vorhanden ist.

# 2.D 
# berechne den Ticketpreis als Mittel über
# die Spalte "fare" in der der Preis, den jede
# Person bezahl hat hinterlegt ist
mean_price = titanic_data['fare'].mean()
print('Der durchschnittliche Ticketpreis ist {}.'\
      .format(mean_price))

# 2.E Optional 
# berecne die Anzahln der Überlebenden als Summe
# über die Salte "survived". Überlebende sind hier
# mit einer "1" markiert, während gestorbene mit
# einer "0" markiert sind
survivors = titanic_data['survived'].sum()
print('{} Personen haben das Unglück mit der Titanic überlebt.'\
     .format(survivors))

# 2.F
lange_liste = list(range(0,100))
listenlaenge = len(lange_liste)
print(listenlaenge)

# +
# 2.G Optional
zahlen_liste = [7, 104, 79, 23, 56]
summen_liste = [] # dies ist eine leere Liste

for zahl in zahlen_liste:
    # berechne die Summe aus Zahl und Summe
    # aller Zahlen in der Liste
    zahl = zahl + sum(zahlen_liste)
    
    # gib die neu berechnete Zahl aus
    print('neue Zahl: {}'.format(zahl))
    
    # füge die neu berechnete Zahl der neuen
    # Liste "summen_liste" hinzu
    summen_liste.append(zahl)
    
print(summen_liste)
# -

# 2.H
lange_liste[0::2] = [0]*50
print(lange_liste)
